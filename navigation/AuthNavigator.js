import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';

// import SplashScreen from '../screens/SplashScreen';
import LoginSelectionScreen from '../screens/LoginSelectionScreen';
import TherapistOnBoardingStep1Screen from '../screens/TherapistOnBoardingStep1Screen';
import TherapistOnBoardingStep2Screen from '../screens/TherapistOnBoardingStep2Screen';
import TherapistOnBoardingStep3Screen from '../screens/TherapistOnBoardingStep3Screen';
import SalonRegistrationScreen from '../screens/SalonRegistrationScreen';
import CustomerLoginScreen from '../screens/CustomerLoginScreen';

const AuthNavigator = createStackNavigator({ 
    // Splash: {
    //     screen: SplashScreen,
    //     navigationOptions: {
    //         headerShown: false
    //     }
    // },
    LoginSelection: {
        screen: LoginSelectionScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    //Customer Navigation
    CustomerLogin: {
        screen: CustomerLoginScreen,
        navigationOptions: {
            headerShown: false
        }
    },

    //Beautician Navigation
    TherapistOnBoardingStep1: {
        screen: TherapistOnBoardingStep1Screen,
        navigationOptions: {
            headerShown: false
        }
    },
    TherapistOnBoardingStep2: {
        screen: TherapistOnBoardingStep2Screen,
        navigationOptions: {
            headerShown: false
        }
    },
    TherapistOnBoardingStep3: {
        screen: TherapistOnBoardingStep3Screen,
        navigationOptions: {
            headerShown: false
        }
    },

    //Salon Navigation
    SalonRegistration: {
        screen: SalonRegistrationScreen,
        navigationOptions: {
            headerShown: false
        }
    },
});

export default AuthNavigator;