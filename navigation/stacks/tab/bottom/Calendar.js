import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Icon } from 'react-native-elements';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View, Dimensions, ScrollView } from 'react-native';

import CalendarScreen from '../../../../screens/CalendarScreen';
import NavigationDrawerStructure from '../../../../components/NavigationDrawerStructure';

const Calendar = createStackNavigator(
  {
    Calendar: {
      screen: CalendarScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Calendar',
        headerLeft: ()=> <NavigationDrawerStructure navigationProps={navigation} />,
        headerStyle: {
          backgroundColor: '#644263',
        },
        headerTintColor: '#fff',
      }),
    },
  }
);

Calendar.navigationOptions = {
    tabBarLabel: 'Calendar',
    tabBarIcon: ({ focused, tintColor }) => (
        (focused) ? 
      <Image
        source={require('../../../../assets/images/icons/calendar-purple.png')}
        style={{ width: '35%' }}
        resizeMode="contain"
      />
      :
      <Image
        source={require('../../../../assets/images/icons/calendar-grey.png')}
        style={{ width: '35%' }}
        resizeMode="contain"
      />
    )
};

export default Calendar;
