import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Icon } from 'react-native-elements';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View, Dimensions, ScrollView } from 'react-native';

import MessagesScreen from '../../../../screens/MessagesScreen';
import NavigationDrawerStructure from '../../../../components/NavigationDrawerStructure';

const Messages = createStackNavigator(
  {
    Messages: {
      screen: MessagesScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Messages',
        headerLeft: ()=> <NavigationDrawerStructure navigationProps={navigation} />,
        headerStyle: {
          backgroundColor: '#644263',
        },
        headerTintColor: '#fff',
      }),
    },
  }
);

Messages.navigationOptions = {
    tabBarLabel: 'Messages',
    tabBarIcon: ({ focused, tintColor }) => (
        (focused) ? 
      <Image
        source={require('../../../../assets/images/icons/message-purple.png')}
        style={{ width: '35%' }}
        resizeMode="contain"
      />
      :
      <Image
        source={require('../../../../assets/images/icons/message-grey.png')}
        style={{ width: '35%' }}
        resizeMode="contain"
      />
    )
};

export default Messages;
