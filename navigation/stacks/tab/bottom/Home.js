import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Icon } from 'react-native-elements';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View, Dimensions, ScrollView } from 'react-native';

import HomeScreen from '../../../../screens/HomeScreen';
import NavigationDrawerStructure from '../../../../components/NavigationDrawerStructure';
import BackButton from '../../../../components/BackButton';

import AvailableBeauticiansScreen from '../../../../screens/AvailableBeauticiansScreen';
import BookingBeauticianScreen from '../../../../screens/BookingBeauticianScreen';


const Home = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: ({ navigation }) => ({
        title: ' ',
        headerLeft: ()=> <NavigationDrawerStructure navigationProps={navigation} />,
        headerStyle: {
          backgroundColor: '#644263',
        },
        headerTintColor: '#fff',
      }),
    },
    // AvailableBeauticians: AvailableBeauticiansScreen
    AvailableBeauticians: {
      screen: AvailableBeauticiansScreen,
      navigationOptions: ({ navigation }) => ({
        // headerLeft: ()=> <BackButton navigation={navigation} />,
        headerStyle: {
          backgroundColor: '#644263',
        },
        headerTintColor: '#fff',
      }),
    },
    BookingBeautician: {
      screen: BookingBeauticianScreen,
      navigationOptions: ({ navigation }) => ({
        // headerLeft: ()=> <BackButton navigation={navigation} />,
        headerStyle: {
          backgroundColor: '#644263',
        },
        headerTintColor: '#fff',
      }),
    },
  }
);

Home.navigationOptions = {
    tabBarLabel: 'Home',
    tabBarIcon: ({ focused, tintColor }) => (
        (focused) ? 
      <Image
        source={require('../../../../assets/images/icons/home-purple.png')}
        style={{ width: '35%' }}
        resizeMode="contain"
      />
      :
      <Image
        source={require('../../../../assets/images/icons/home-grey.png')}
        style={{ width: '35%' }}
        resizeMode="contain"
      />
    )
};

export default Home;
