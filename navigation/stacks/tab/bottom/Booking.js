import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Icon } from 'react-native-elements';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View, Dimensions, ScrollView } from 'react-native';

import BookingScreen from '../../../../screens/BookingScreen';
import NavigationDrawerStructure from '../../../../components/NavigationDrawerStructure';

const Booking = createStackNavigator(
  {
    Booking: {
      screen: BookingScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Booking',
        headerLeft: ()=> <NavigationDrawerStructure navigationProps={navigation} />,
        headerStyle: {
          backgroundColor: '#644263',
        },
        headerTintColor: '#fff',
      }),
    },
  }
);

Booking.navigationOptions = {
    tabBarLabel: ' ',
    tabBarIcon: ({ focused, tintColor }) => (
      <Image
        source={require('../../../../assets/images/icons/booknow.png')}
        style={{ width: '120%',marginBottom:30}}
        resizeMode="contain"
      />
    ),
    tabBarVisible: false,
    
      // let tabBarVisible = true;
      // if (navigation.state.index > 0) {
      //   tabBarVisible = false;
      // }
    
      // return {
      //   tabBarVisible,
      // };

};

export default Booking;
