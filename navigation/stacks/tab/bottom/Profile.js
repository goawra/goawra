import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Icon } from 'react-native-elements';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View, Dimensions, ScrollView } from 'react-native';

import ProfileScreen from '../../../../screens/ProfileScreen';
import NavigationDrawerStructure from '../../../../components/NavigationDrawerStructure';

const Profile = createStackNavigator(
  {
    Profile: {
      screen: ProfileScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Profile',
        headerLeft: ()=> <NavigationDrawerStructure navigationProps={navigation} />,
        headerStyle: {
          backgroundColor: '#644263',
        },
        headerTintColor: '#fff',
      }),
    },
  }
);

Profile.navigationOptions = {
    tabBarLabel: 'Profile',
    tabBarIcon: ({ focused, tintColor }) => (
        (focused) ? 
      <Image
        source={require('../../../../assets/images/icons/profile-purple.png')}
        style={{ width: '35%' }}
        resizeMode="contain"
      />
      :
      <Image
        source={require('../../../../assets/images/icons/profile-grey.png')}
        style={{ width: '35%' }}
        resizeMode="contain"
      />
    )
};

//Hide whole tabBar when this active
// Profile.navigationOptions = ({ navigation }) => {
//   let { routeName } = navigation.state.routes[navigation.state.index];
//   let navigationOptions = {};

//   if (routeName === '4') { //route name
//     navigationOptions.tabBarVisible = false;
//   }

//   return navigationOptions;
// };

export default Profile;
