import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Icon } from 'react-native-elements';

import WalletScreen from '../../../screens/WalletScreen';
import NavigationDrawerStructure from '../../../components/NavigationDrawerStructure';

const Wallet = createStackNavigator(
  {
    Wallet: {
      screen: WalletScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Wallet',
        headerLeft: ()=> <NavigationDrawerStructure navigationProps={navigation} />,
        headerStyle: {
          backgroundColor: '#644263',
        },
        headerTintColor: '#fff',
      }),
    },
  }
);

Wallet.navigationOptions = {
  drawerLabel: 'Wallet',
  drawerIcon: ({ tintColor }) => (
    <Icon
      name="list"
      size={30}
      iconStyle={{
        width: 30,
        height: 30,
      }}
      type="material"
      color={tintColor}
    />
  ),
};

export default Wallet;
