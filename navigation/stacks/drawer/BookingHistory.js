import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Icon } from 'react-native-elements';
import {
  Dimensions,
  AppRegistry,
  StyleSheet,
  View,
  Text,
  Image,
  Button,
  Alert,
  TouchableWithoutFeedback,
  TouchableOpacity
} from 'react-native';

import BookingHistoryScreen from '../../../screens/BookingHistoryScreen';
// import BookingHistoryDetailsScreen from '../../../screens/BookingHistoryDetailsScreen';
import NavigationDrawerStructure from '../../../components/NavigationDrawerStructure';


const BookingHistory = createStackNavigator(
  {
    BookingHistory: {
      screen: BookingHistoryScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Booking History',
        headerLeft: ()=> <NavigationDrawerStructure navigationProps={navigation} />,
        // headerLeft: ()=><TouchableOpacity onPress={() => { navigation.goBack() }}>
        // <Icon
        //   name="chevron-left"
        //   size={30}
        //   type="entypo"
        //   color='#fff'
        //   iconStyle={{ paddingLeft: 10 }}
        //   // onPress={navigation.toggleDrawer}
        // />
        // </TouchableOpacity>,
        
        headerStyle: {
          backgroundColor: '#644263',
        },
        headerTintColor: '#fff',
      }),
    },
  }
);

BookingHistory.navigationOptions = {
  drawerLabel: 'Booking History',
  drawerIcon: ({ tintColor }) => (
    <Icon
      name="list"
      size={30}
      iconStyle={{
        width: 30,
        height: 30,
      }}
      type="material"
      color={tintColor}
    />
  ),
};

export default BookingHistory;
