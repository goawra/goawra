import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { Icon } from 'react-native-elements';

import NotificationScreen from '../../../screens/NotificationScreen';
import NavigationDrawerStructure from '../../../components/NavigationDrawerStructure';

const Notification = createStackNavigator(
  {
    Notification: {
      screen: NotificationScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Notifications',
        headerLeft: ()=> <NavigationDrawerStructure navigationProps={navigation} />,
        headerStyle: {
          backgroundColor: '#644263',
        },
        headerTintColor: '#fff',
      }),
    },
  }
);

Notification.navigationOptions = {
  drawerLabel: 'Notifications',
  drawerIcon: ({ tintColor }) => (
    <Icon
      name="list"
      size={30}
      iconStyle={{
        width: 30,
        height: 30,
      }}
      type="material"
      color={tintColor}
    />
  ),
};

export default Notification;
