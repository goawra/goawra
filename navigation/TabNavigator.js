import React from 'react';
import { createBottomTabNavigator, BottomTabBar } from 'react-navigation-tabs';
import { Icon } from 'react-native-elements';
import { View, Text, TouchableOpacity,Image,Button,Dimensions } from 'react-native';

import Home from './stacks/tab/bottom/Home';
import Calendar from './stacks/tab/bottom/Calendar';
import Messages from './stacks/tab/bottom/Messages';
import Booking from './stacks/tab/bottom/Booking';
import Profile from './stacks/tab/bottom/Profile';
const { width, height } = Dimensions.get('window');
const bottomBarHeight = 60;

const TabBarComponent = props => <BottomTabBar {...props} />;

const TabNavigator = createBottomTabNavigator(
  {
    HomeTab: {
      screen: Home,
    },
    CalendarTab: {
      screen: Calendar,
      path: '/calendar',
    },
    BookingTab: {
      screen: Booking,
      path: '/booking',
    },
    MessagesTab: {
      screen: Messages,
      path: '/messages',
    },
    ProfileTab: {
      path: '/profile',
      screen: Profile,
    },
  },
  {
    initialRouteName: 'HomeTab',
    animationEnabled: false,
    swipeEnabled: true,
    // Android's default option displays tabBars on top, but iOS is bottom
    tabBarPosition: 'bottom',
    tabBarOptions: {
      activeTintColor: '#644263',
      // Android's default showing of icons is false whereas iOS is true
      showIcon: true,
      tabStyle: {
        // paddingVertical: 10,
      },
      style:{
        // padding:10,
        height: bottomBarHeight,
        shadowColor: '#000',
        shadowOffset: {
          width: 1,
          height: 1,
        },
      backgroundColor: 'transparent',
      shadowOpacity: 0.25,
      shadowRadius: 10,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    }
      
    },
    // tabBarComponent: props => {
    //   const currentIndex = props.navigation.state.index;

    //   return (
    //     <TabBarComponent
    //       {...props}
    //       // style={
    //       //   currentIndex === 3 && {
    //       //     backgroundColor: 'rgba(47,44,60,1)',
    //       //     borderTopWidth: 0,
    //       //   }
    //       // }
    //       navigation={{
    //         ...props.navigation,
    //         state: {
    //           ...props.navigation.state,
    //           routes: props.navigation.state.routes,
    //         },
    //       }}
    //     />
    //   );
    // },
  }
);

export default TabNavigator;
