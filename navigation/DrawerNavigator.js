import React from 'react';
import { View, Image, Dimensions,Text, Button,StyleSheet,TouchableOpacity,ScrollView,SafeAreaView } from 'react-native';
import {
  createDrawerNavigator,
  DrawerNavigatorItems
} from 'react-navigation-drawer';

import AsyncStorage from '@react-native-community/async-storage';

const WINDOW_WIDTH = Dimensions.get('window').width;

//Drawer Item list
import Home from './stacks/drawer/Home';
import BookingHistory from './stacks/drawer/BookingHistory';
import Schedule from './stacks/drawer/Schedule';
import Wallet from './stacks/drawer/Wallet';
import Notification from './stacks/drawer/Notification';

import CustomSidebarMenu from '../components/CustomSidebarMenu';

import MainNavigator from './MainNavigator';
// import NavigationDrawerStructure from '../components/NavigationDrawerStructure';

const customContentComponent = props => (
    // const userData = await AsyncStorage.getItem('@userData');
    // let user = JSON.parse(userData);
    // this.setState(user);
<ScrollView>
  <SafeAreaView
    style={{ flex: 1, height: '100%' }}
    forceInset={{ top: 'always', horizontal: 'never' }}
  >
    <View style={styles.avatar}>
        <Image 
            source={require('../assets/images/avatar.png')}
           style={{ width: '70%' }}
        />
        <TouchableOpacity>
            <Text>Edit Profile</Text>
        </TouchableOpacity>
        <Text style={{}}>Angel Lockring</Text>
    </View>
    <View style={{ marginLeft: 10 }}>
      <DrawerNavigatorItems {...props} />
    </View>
  </SafeAreaView>
  </ScrollView>
);

 

const DrawerNavigator = createDrawerNavigator({

    Home: {
      path: '/home',
      screen: MainNavigator
    },
    BookingHistory: {
      path: '/booking_history',
      screen: BookingHistory,
    },
    Schedule: {
      path: '/schedule',
      screen: Schedule,
    },
    Wallet: {
      path: '/wallet',
      screen: Wallet,
    },
    Notification: {
      path: '/notification',
      screen: Notification,
    },
  },
  {
    initialRouteName: 'Home',
    contentOptions: {
      activeTintColor: '#548ff7',
      activeBackgroundColor: 'transparent',
      inactiveTintColor: '#ffffff',
      inactiveBackgroundColor: 'transparent',
      backgroundColor: '#43484d',
      labelStyle: {
        fontSize: 15,
        marginLeft: 0,
      },
    },
    // contentComponent: CustomDrawerContentComponent,
    contentComponent: CustomSidebarMenu,
    drawerWidth: Math.min(WINDOW_WIDTH * 0.8, 300),
    
  }
);

export default DrawerNavigator;

const styles = StyleSheet.create({
  avatar: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  drawerContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  userInfoSection: {
    paddingLeft: 20,
  },
  title: {
    marginTop: 20,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },

});