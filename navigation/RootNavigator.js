import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import DrawerNavigator from './DrawerNavigator';
import AuthNavigator from './AuthNavigator';
import SplashScreen from '../screens/SplashScreen';

const SplashNavigator = createStackNavigator({ 
  Splash: {
      screen: SplashScreen,
      navigationOptions: {
          headerShown: false
      }
  }
});

export default createAppContainer(
  createSwitchNavigator({
      Dashboard: DrawerNavigator,
      Auth: AuthNavigator,
      Splash: SplashNavigator,
    },
    {
      initialRouteName: 'Splash',
    }
  )
);