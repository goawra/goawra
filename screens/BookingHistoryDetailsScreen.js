import React from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    Dimensions,
    StyleSheet,
    StatusBar,
    Image,
    ImageBackground
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const BookingHistory = ({navigation}) => {
    // const { colors } = useTheme();
    // AsyncStorage.removeItem('@userData')

// setTimeout(() => {
//     navigation.navigate('Home');
// }, 1000);
// }, 3000);


// this.navigation.navigate({ screen: 'LoginSelection' });
// console.log(navigation);

    return (
      <View style={styles.container}>
            <Text>Booking History Screen</Text>
      </View>
    );
};

export default BookingHistory;


const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor: '#644263'
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
},
  header: {
      flex: 2,
      justifyContent: 'center',
      alignItems: 'center'
  },
  footer: {
      flex: 1,
      backgroundColor: '#fff',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      paddingVertical: 50,
      paddingHorizontal: 30
  },
  logo: {
      width: 208,
      height: 185,
      flex: 1,
    // alignItems: "center",
    // // resizeMode: "cover",
    // justifyContent: "center"
    
    resizeMode:'contain'
  },
  title: {
      color: '#05375a',
      fontSize: 30,
      fontWeight: 'bold'
  },
  text: {
      color: 'grey',
      marginTop:5
  },
  button: {
      alignItems: 'flex-end',
      marginTop: 30
  },
  signIn: {
      width: 150,
      height: 40,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 50,
      flexDirection: 'row'
  },
  textSign: {
      color: 'white',
      fontWeight: 'bold'
  }
});

