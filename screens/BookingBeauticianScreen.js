
import React, { Component } from 'react';
import { StyleSheet, View, ScrollView,SafeAreaView,FlatList,TouchableOpacity,TouchableHighlight,Dimensions,ActivityIndicator } from 'react-native';
import { ListItem, Avatar, Card, Image, Button } from 'react-native-elements';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Rating, AirbnbRating } from 'react-native-elements';

import AppHeader from '../components/AppHeader';
import Text from '../components/CustomText';
import FilledButton from '../components/FilledButton';
import Filter from '../components/Filter';
import api from '../utils/api';

const DEVICE_WIDTH = Dimensions.get('window').width;
const CONTAINER_WIDTH = DEVICE_WIDTH / 2 - 8;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const CONTAINER_HEIGHT = DEVICE_HEIGHT / 2 - 8;

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
  },
];
const Item = ({ title }) => (
  <View style={styles.item}>
    <Text style={styles.title}>{title}</Text>
  </View>
);

export default class BookingBeauticianScreen extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      profile: [],
      categories: [
        {
          id:1,
          avatar: require('../assets/images/hair.jpg'),
          service: 'HAIR'
        },
        {
          id:2,
          avatar: require('../assets/images/nails.jpg'),
          service: 'NAILS'
        },
        {
          id:3,
          avatar: require('../assets/images/makeup.jpg'),
          service: 'MAKEUP'
        },
        {
          id:4,
          avatar: require('../assets/images/massage.jpg'),
          service: 'MASSAGE'
        },
        {
          id:5,
          avatar: require('../assets/images/footspa.jpg'),
          service: 'FOOT SPA'
        },
      ],
      beauticians: [
        {
          id:1,
          avatar: require('../assets/images/pedroAcre.png'),
          FullName: 'Pedro Acre'
        },
        {
          id:2,
          avatar: require('../assets/images/pedroAcre.png'),
          FullName: 'Angel Lockring'
        },
        {
          id:3,
          avatar: require('../assets/images/pedroAcre.png'),
          FullName: 'Acre Pedro'
        },
        {
          id:4,
          avatar: require('../assets/images/pedroAcre.png'),
          FullName: 'Lockring Angel'
        },
        {
          id:5,
          avatar: require('../assets/images/pedroAcre.png'),
          FullName: 'Pedro Acre'
        },

      ],
    }
    
  }

  async componentDidMount() {

    // await api.get('/user/beautician')
    // .then(async (response) =>  {
    //     if (response.data.services){
    //         this.setState({ 'beauticians': response.data.services });
    //         await this.setState({ isLoading: !this.state.isLoading })
    //     } else {
    //         await this.setState({ isLoading: !this.state.isLoading })
    //     }
    // }).catch((error)=>{
    //     console.log(error.response);
    //     alert(error.message);
    //  });
    this.getProfile();
  }

  getProfile = async () => {
    await api.get('/user/profile/5f9d6cb4e8dc2ecddb0d63af')
    .then(async (response) =>  {
        if (response.data.profile){
            this.setState({ 'profile': response.data.profile });
            await this.setState({ isLoading: false })
            console.log('DATA---------- LOADED');
            console.log('loading----------'+this.state.isLoading);
            console.log(response.data.profile.operateServices[0].subCategory);
        } else {
            await this.setState({ isLoading: true })
        }
        
        
    }).catch((error)=>{
        console.log(error.response);
        alert(error.message);
     });
  }
  // static navigationOptions = ({ navigation }) => ({
  //   // header: <AppHeader showBadge={true} leftAction={() => { return null }} leftLabel={' '} rightAction={() => navigation.navigate('ViewCart')} rightLabel={<Ionicons name={Platform.OS === 'ios' ? 'ios-cart' : 'md-cart'} size={25} />} />
  //   header: <AppHeader/>
  // });
  render() {
    

    const { navigation } = this.props;
    
    const lastIndex = this.state.beauticians.slice(-1)[0]
  
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={{paddingBottom:50}}>
          <View style={{flexDirection: 'row', width: DEVICE_WIDTH}}>
            <View style={[styles.viewStyle,styles.profile, {flex: 1, flexDirection: 'row'}]}>
              <View style={styles.twoColumn}>
                <Image style={{justifyContent: 'center',
                  borderRadius:12,
                  // borderTopLeftRadius:12,
                  // borderTopRightRadius:12,
                  height:250
                }}
                  // source={require('../assets/images/pedroAcre.png')}
                  source={{uri:this.state.profile.photoUrl}}
                />
              </View>
              <View style={styles.twoColumn}>
                <View>
                  <Text>{this.state.profile.FullName}</Text>
                </View>
                <View>
                <Text>Rating:</Text>
                  <Rating
                    style={{ paddingVertical: 10 }}
                    type='custom'
                    imageSize={20}
                    tintColor='#EFEFEF'
                    ratingColor='#FFA800'
                  />
                </View>
                <View>
                  <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae posuere quam. Suspendisse euismod ipsum eget ligula fermentum, vehicula lobortis mi tristique.</Text>
                </View>
                <View><Text>09053306410</Text></View>
                <View>
                  <Text>Blk 1 Lot 2 Tree St.
                      Quezon City, Metro Manila</Text>
                </View>
                <View style={styles.scheduleBtn}>
                    {/* <TouchableOpacity onPress={this.onStartCardEntry.bind(this, (grandTotal + this.props.serviceCharge))} disabled={this.state.dataLoaded ? false : true}> */}
                    <TouchableOpacity onPress={{}}>
                      <Text style={{color:'#fff'}}>SCHEDULE BOOKING</Text>
                    </TouchableOpacity>
                </View>
              </View>
              
            </View>
          </View>
          <View style={[styles.viewStyle,styles.menu, {flex: 1, flexDirection: 'row'}]}>
            <View style={styles.childStyle}>
              <View>
                {/* {this.state.profile.operateServices[0].subCategory} */}
              </View>
                <Image style={{justifyContent: 'center',
                  borderTopLeftRadius:12,
                  borderTopRightRadius:12,
                  height:250
                }}
                  // source={require('../assets/images/pedroAcre.png')}
                  source={require('../assets/images/pedroAcre.png')}
                />
              </View>
          </View>
        </ScrollView>
        
        </View>

        

      
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // flexDirection:'row',
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: '#F5FCFF',
  },
  column: {
    flexDirection:'column',
    justifyContent:'space-between',
    alignItems:'center',
    // height:200,
    width:"50%"
  },
  space_between_columns:{
    width:15
  },
  box: {
    height:50,
    // width:"100%",
    width:"100%",
    backgroundColor: 'green',
  },
  nameContainer: {
    // position: 'absolute',
    padding:10,
    backgroundColor: '#644263',
    width:"100%",
    borderBottomLeftRadius:12,borderBottomRightRadius:12
  },
    name: {
    // position: 'absolute',
    fontWeight: '400',
    fontSize:18,
    bottom:0,
    color:'#fff',
    textTransform:'uppercase',
    textAlign:'center',
  },
  viewStyle: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    padding: 10,

    justifyContent: 'space-between'
  },
  childStyle: {
    width: '49%',
    marginBottom: 10,
    // marginBottom: space
  },
  profile: {
    flex: 1,
    backgroundColor: '#EFEFEF',
    alignItems: 'flex-start', // if you want to fill rows left to right
  },
  menu:{
    flex:1,
    backgroundColor: '#FFFFFF'
  },
  scheduleBtn: {
    backgroundColor: '#644263',
    padding:15,
    width: "100%",
    justifyContent: 'center',
    textAlign:'center',
    alignItems: 'center'
  },
  twoColumn: {
    width: '50%', // is 50% of container width
    padding: 5
  }
});
