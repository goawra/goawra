import React, { Component } from 'react'
import {
    View,
    TextInput,
    Dimensions,
    Image,
    StyleSheet,
    TouchableOpacity,
    Button
} from 'react-native';
import BulletNavigation from '../components/BulletNavigation';
import Text from '../components/CustomText';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    paragraph: {
        marginBottom: hp('3.5%'),
        textAlign: 'center'
    },
    heading: {
        fontSize: 30,
        color: 'white'
    },
    heading2: {
        fontSize: 25,
        color: 'white'
    }
})

export default class TherapistOnBoardingStep1Screen extends Component {
    constructor(props) {
        super()
    }

    render() {

        return (
            <View style={{ backgroundColor: '#634262',alignItems: 'center', justifyContent: 'center', width: DEVICE_WIDTH, height: DEVICE_HEIGHT, borderBottomWidth: wp('1.8%'), borderBottomColor: "#333333" }}>
                <View>
                    <TouchableOpacity style={{ width: DEVICE_WIDTH - wp('15%'), marginTop: hp('1%') }}>
                        <View style={{ alignItems: 'center' }}>
                            <Text style={[styles.heading, { textAlign: 'center', marginBottom: hp('3.5%') }]}>Welcome to GoAwra</Text>
                            <Text style={[styles.heading2, { textAlign: 'center', marginBottom: hp('3.5%') }]}>Let's get started!</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={{ position: 'absolute', bottom: 0, width: DEVICE_WIDTH }}>
                    <BulletNavigation next="TherapistOnBoardingStep2" max="3" active="1" navigation={this.props.navigation} back="true" />
                </View>
            </View>
        )
    }
}
