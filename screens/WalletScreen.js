import React, { Component } from 'react';
import { StyleSheet, View, Dimensions,SafeAreaView, TouchableOpacity } from 'react-native';
import Text from '../components/CustomText';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
// import {Card} from '../components/Card';
import {Image,Card } from 'react-native-elements';
import { MaterialCommunityIcons } from '@expo/vector-icons';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;


export default class WalletScreen extends Component {

  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={{flex: 1}}>
          <View style={styles.sectionOne}>

            <View style={styles.sectionOneBalanceContainer}>
              <Text style={{}}>php</Text>
              <Text style={styles.sectionOneBalanceAmount}>2000</Text>
              <Text style={{}}>Balance</Text>
            </View>
            
          </View>
          <View style={styles.sectionTwo}>
            <Card containerStyle={styles.card}>
            {/* <View style={styles.walletChoices}>
              <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
              <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
              <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
            </View> */}
              {/* <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
              <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
              <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} /> */}
              <View style={styles.walletChoices}>
                <TouchableOpacity>
                  <MaterialCommunityIcons name="clock-outline" size={50} color="black" />
                  <Text style={styles.walletChoicesText}> Pay </Text>
                </TouchableOpacity>
                
                <TouchableOpacity>
                  <MaterialCommunityIcons name="clock-outline" size={50} color="black" />
                  <Text style={styles.walletChoicesText}> Pay </Text>
                </TouchableOpacity>

                <TouchableOpacity>
                  <MaterialCommunityIcons name="clock-outline" size={50} color="black" />
                  <Text style={styles.walletChoicesText}> Pay </Text>
                </TouchableOpacity>
              </View>
           
            {/* <Image
                source={{uri: item.promoImages}}
                style={{ height: 110, width: 330, marginTop:20,resizeMode:'contain', borderRadius:30}}
              /> */}
            </Card>
            <Text style={{ fontSize: 23 }}> Wallet Screen </Text>
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingTop: 20,
    // alignItems: 'center',
    // marginTop: 50,
    // justifyContent: 'center',
  },
  sectionOne: {
    backgroundColor: "#644263",
    width: wp('100%'),
    height: hp('30%')
    // height: DEVICE_HEIGHT /2,
    // width: DEVICE_WIDTH
    
  },
  sectionOneBalanceContainer: {
    alignItems: 'center',
    // marginTop: 50,
    justifyContent: 'center',
    
  },
  sectionOneBalanceAmount: {
    color: '#fff',
    fontSize:60
  },
  sectionTwo: {
    width: wp('100%'),
    height: hp('70%')
    // height: DEVICE_HEIGHT /2,
    // width: DEVICE_WIDTH

  },
  card: {
    
    padding:20,
    
    backgroundColor: 'white',
    borderRadius: 16,
    shadowOpacity: 0.2,
    shadowRadius: 4,
    shadowColor: 'black',
    shadowOffset: {
      height: 0,
      width: 0,
    },
    elevation: 1,
    // width: wp('80%'),
    marginTop: -20
    // backgroundColor: '#000'
  },
  walletChoices: {
   
    flexDirection: 'row',
    justifyContent: 'space-around',
  
   
    // justifyContent: 'center',
  },
  walletChoicesText: {
    fontSize: 15,
    textAlign: 'center'
  }
});