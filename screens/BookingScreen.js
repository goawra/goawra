
import React, { Component } from 'react'
import { Image, Platform, StyleSheet, TouchableOpacity, View, Dimensions, ScrollView,TextInput,ActivityIndicator } from 'react-native';
// import AppHeader from '../components/AppHeader';

import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import * as Location from 'expo-location';
import Text from '../components/CustomText';
import { CurrentLocationButton } from '../components/CurrentLocationButton';
import { MaterialIcons, Entypo } from '@expo/vector-icons';
{/* <Entypo name="location-pin" size={24} color="green" /> */}

const DEVICE_WIDTH = Dimensions.get('window').width;
const CONTAINER_WIDTH = DEVICE_WIDTH / 2 - 8;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const CONTAINER_HEIGHT = DEVICE_HEIGHT / 2 - 8;

const LATITUDE_DELTA = 0.0922;
// const LONGITUDE_DELTA = LATITUDE_DELTA + (DEVICE_WIDTH / DEVICE_HEIGHT);
const LONGITUDE_DELTA = Dimensions.get("window").width / Dimensions.get("window").height * 0.00522;
console.log('long DELTA: '+ Dimensions.get("window").width / Dimensions.get("window").height * 0.00522);

// const destinationMarker = () => (
//   <Marker
//       coordinate={toLocation}
//   >
//       <View
//           style={{
//               height: 40,
//               width: 40,
//               borderRadius: 20,
//               alignItems: 'center',
//               justifyContent: 'center',
//               backgroundColor: COLORS.white
//           }}
//       >
//           <View
//               style={{
//                   height: 30,
//                   width: 30,
//                   borderRadius: 15,
//                   alignItems: 'center',
//                   justifyContent: 'center',
//                   backgroundColor: COLORS.primary
//               }}
//           >
//               {/* <Image
//                   source={icons.pin}
//                   style={{
//                       width: 25,
//                       height: 25,
//                       tintColor: COLORS.white
//                   }}
//               /> */}
//           </View>
//       </View>
//   </Marker>
// )

export default class BookingScreen extends Component {
  constructor(props) {
      super(props)
      this.state = {
        // latitude:14.696105,
        // longitude:121.085603,
        currentLocation: [],
        coordinates: [],
        beautician: {
          FullName: 'Pedro Arce'
        },
        region: null,

      }  
      this._getLocationAsync();
      // this.mapView = React.createRef();
      
  }
  _getCoords = () => {
    this.map.animateCamera({
      center: 
      this.state.region,
      heading: 0,
      // pitch: 90,
    });
    // let location = await Location.getCurrentPositionAsync(
    //   // accuracy: Location.Accuracy.BestForNavigation
    //   (position) => {
    //             var initialPosition = JSON.stringify(position.coords);
    //             this.setState({position: initialPosition});
    //             let tempCoords = {
    //                 latitude: Number(position.coords.latitude),
    //                 longitude: Number(position.coords.longitude)
    //             }
    //             this.map.animateCamera(tempCoords, 1);
    //           }, function (error) { alert(error) },
    // );
    // console.log('derald pogi');
    // navigator.geolocation.getCurrentPosition(
    //     (position) => {
    //         var initialPosition = JSON.stringify(position.coords);
    //         this.setState({position: initialPosition});
    //         let tempCoords = {
    //             latitude: Number(position.coords.latitude),
    //             longitude: Number(position.coords.longitude)
    //         }
    //         this.map.animateCamera(tempCoords, 1);
    //       }, function (error) { alert(error) },
    //  );
  };
  

_getLocationAsync = async () => {
  let { status } = await Location.requestPermissionsAsync();
  if (status !== 'granted') {
    setErrorMsg('Permission to access location was denied');
  }

  let location = await Location.getCurrentPositionAsync({
    // accuracy: Location.Accuracy.BestForNavigation
  });

  let region = {
    latitude: location.coords.latitude,
    longitude: location.coords.longitude,
    // latitudeDelta: 0.045,
    // longitudeDelta: 0.045
    latitudeDelta: 0.00922,
    longitudeDelta: LONGITUDE_DELTA
  };
  console.log(region)

  this.setState({region});

  let geoCode = await Location.reverseGeocodeAsync(location.coords);
  console.log(geoCode);

  await this.setState({
    currentLocation: geoCode[0].name +", "+ geoCode[0].city +", "+ geoCode[0].region
  });
}
  
async _onChangeMarker (markerData) {
  // this.setState({ openedMarker: markerData });
  // this.refs.map.animateToRegion({
  //     latitude: parseFloat(markerData.latitude),
  //     longitude: parseFloat(markerData.longitude),
  //     latitudeDelta: 0.0043,
  //     longitudeDelta: 0.0034
  // });
  console.log(markerData)
  this.setState({region:{
    latitude: markerData.latitude,
    longitude: markerData.longitude,
    latitudeDelta: 0.00922,
    longitudeDelta: LONGITUDE_DELTA
  }});

  let geoCode = await Location.reverseGeocodeAsync(markerData);
  console.log(geoCode);

  await this.setState({
    currentLocation: geoCode[0].name +", "+ geoCode[0].city +", "+ geoCode[0].region
  });
}

// async componentDidMount(){

  // let { status } = await Location.requestPermissionsAsync();
  // if (status !== 'granted') {
  //   setErrorMsg('Permission to access location was denied');
  // }


  // let location = await Location.getCurrentPositionAsync({
  //   // accuracy: Location.Accuracy.BestForNavigation
  // });
  // console.log(location);
  
  // await this.setState({
  //   coordinates: location.coords
  // });

 

// centerMap(){
//   const {
//     latitude,
//     longitude,
//     latitudeDelta,
//     longitudeDelta,
//   } = this.state.region;

//   this.map.animateToRegion({
//     latitude,
//     longitude,
//     latitudeDelta,
//     longitudeDelta,
//   })
  
// }
//  calculateAngle(coordinates) {
//   let startLat = coordinates[0]["latitude"]
//   let startLng = coordinates[0]["longitude"]
//   let endLat = coordinates[1]["latitude"]
//   let endLng = coordinates[1]["longitude"]
//   let dx = endLat - startLat
//   let dy = endLng - startLng

//   return Math.atan2(dy, dx) * 180 / Math.PI
// }

// zoomIn() {
//   let newRegion = {
//       latitude: region.latitude,
//       longitude: region.longitude,
//       latitudeDelta: region.latitudeDelta / 2,
//       longitudeDelta: region.longitudeDelta / 2
//   }

//   setRegion(newRegion)
//   mapView.current.animateToRegion(newRegion, 200)
// }

// zoomOut() {
//   let newRegion = {
//       latitude: region.latitude,
//       longitude: region.longitude,
//       latitudeDelta: region.latitudeDelta * 2,
//       longitudeDelta: region.longitudeDelta * 2
//   }

//   setRegion(newRegion)
//   mapView.current.animateToRegion(newRegion, 200)
// }
// onRegionChange(region) {
//   this.setState({ region });
// }
  
  render(){
    
    return (
      
      this.state.region ? (
        <View style={styles.container}>
          <MapView.Animated
            ref={map => { this.map = map }}
            style={styles.mapStyle}
            initialRegion={this.state.region}
            showsCompass={true}
            rotateEnabled={false}
            showsUserLocation={true}
            provider={PROVIDER_GOOGLE}
          >
            <MapView.Marker draggable
              coordinate={this.state.region}
              // onDragEnd={(e) =>  console.log(e.nativeEvent.coordinate)
                
              //   // this.setState({ x: e.nativeEvent.coordinate })
              // }
              onDragEnd={e => { this._onChangeMarker(e.nativeEvent.coordinate)
                console.log('dragEnd', e.nativeEvent.coordinate);
              }}
            >
              <Entypo name="location-pin" size={30} color="#644263" />
            </MapView.Marker>
          </MapView.Animated>
          
          <View style={styles.locationInputContainer}>
            <View style={styles.locationWrapper}>
              <Entypo name="location-pin" size={25} color="#644263"/>
              <TextInput
                style={styles.locationInput}
                numberOfLines={1}
                ellipsizeMode='tail'
                value={this.state.currentLocation.toString()}
                // value="LOREM IPSUM Lorem Ipsum LOREM IPSUM Lorem Ipsum LOREM IPSUM Lorem Ipsum LOREM IPSUM Lorem Ipsum dajshd ajhsda sjd"
               
              />
            </View>
            
            {/* <View style={{backgroundColor: '#E0E0E0'}}>

              <TouchableOpacity>

              </TouchableOpacity>
            </View> */}
          </View>
          <View style={styles.beauticianContainer}>
            <View style={{width:'20%',alignContent:'center'}}>
              <Image 
              source={require('../assets/images/scissorComb.png')}
            />
            </View>
            <View style={{width:'60%',alignContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:19}}>{this.state.beautician.FullName}</Text>
            </View>
            <View style={{width:'20%',alignContent:'center',alignItems:'center'}}>
              <Text style={styles.priceAndMinutes}>{'\u20B1'}500</Text>
              <Text style={{fontSize:11}}>15 mins</Text>
            </View>
            
          </View>
          <TouchableOpacity style={styles.bookNowButton}

          >
            <Text style={styles.bookNowButtonText}>Book Now</Text>
          </TouchableOpacity>
          {/* <CurrentLocationButton onPress={this._getCoords.bind(this)}/> */}
        </View>
        ) : (
          <View style={styles.gifContainer}>
              <ActivityIndicator size="large" color="#644263" />
          </View>
        )
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  mapStyle: {
    flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    ...StyleSheet.absoluteFillObject,
  },
  bookNowButton: {
    position: 'absolute',
    backgroundColor: '#644263',
    width: '80%',
    bottom: 50,
    padding: 20,
    alignItems: 'center',
    borderRadius: 10
  },
  bookNowButtonText: {
    color: '#fff',
    fontSize: 22,
    textTransform: 'uppercase',
    fontWeight: '400',
  },
  locationInputContainer: {
    backgroundColor: '#fff',
    width: '80%',
    position:'absolute', 
    top: 50,
    padding: 20
    // height: 150
  },
  locationWrapper: {
    flexDirection:'row',
    flexWrap:'wrap',
    borderBottomWidth: 1,
    borderBottomColor: '#644263'
  },
  locationInput: {
    paddingVertical: 8,
    fontSize: 15,
    color: '#644263',
    width: '90%'
  },
  gifContainer: {
    flex: 1,
    marginTop:20,
    flexDirection: 'column',
    alignItems: 'center',
  },
  beauticianContainer: {
    position: 'absolute',
    backgroundColor: '#fff',
    width: '80%',
    bottom: 130,
    padding: 20,
    // paddingVertical: 20,
    borderRadius: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  priceAndMinutes: {
    fontSize: 21,
    fontWeight:'bold'
  },
  colThreeContainer: {
    width: '33.33%',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
