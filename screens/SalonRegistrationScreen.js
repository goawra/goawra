
import React, { Component } from 'react'
import {
    Platform,
    View,
    TextInput,
    Dimensions,
    Image,
    StyleSheet,
    TouchableOpacity,
    FlatList,
    Button,
    ActivityIndicator,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
// import { Picker } from '@react-native-community/picker';
import RNPickerSelect, { defaultStyles } from 'react-native-picker-select';
// import { Ionicons } from '@expo/vector-icons'
import { MaterialCommunityIcons } from '@expo/vector-icons';

import Text from '../components/CustomText';

import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import api from '../utils/api';
import {RegistrationComplete} from '../components/RegistrationComplete';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
    //   fontSize: 16,
    // paddingVertical: 1,
    //    paddingHorizontal: 10,
    //   color: 'black',
    //   paddingRight: 10, // to ensure the text is never behind the icon
    },
    inputAndroid: {
    //   fontSize: 16,
    //   paddingHorizontal: 10,
    //   paddingVertical: 8,
    //   color: 'black',
    //   paddingRight: 10, // to ensure the text is never behind the icon
    },
  });

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    inputView:{
        backgroundColor:"#E5E5E5",
        borderRadius:5,
        marginBottom:10,
        justifyContent: "center",
        padding:15,
        height:(Platform.OS === "ios" ? 'auto': 50)
        
      },
      inputViewAndroid:{
        backgroundColor:"#E5E5E5",
        borderRadius:5,
        marginBottom:10,
        justifyContent: "center",
        padding:15,
        paddingLeft:5,
        height:(Platform.OS === "ios" ? 'auto': 50)
        
      },
      inputText:{
        // height:50,
        fontSize:15,
        color: '#000',
      },
      loginBtn:{
        width:"100%",
        backgroundColor:"#A375A3",
        fontSize:18,
        borderRadius:5,
        height:50,
        alignItems:"center",
        justifyContent:"center",
        marginTop:10,
        marginBottom:10
      },
      loginText: {
        color: 'white',
        letterSpacing: 2,
      },
    paragraph: {
        marginBottom: hp('3.5%'),
        textAlign: 'center'
    },
    heading: {
        fontSize: 24,
        color: 'white'
    },
    gifContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
});



class SalonRegistrationScreen extends Component {
    constructor(props) {
        super();

        this.state = {
            isComplete: false,
            firstName: '',
            lastName: '',
            contactNumber: '',
            email: '',
            address: '',
            address2: '',
            city: '',
            postal_code: '',
            storeName:''
        }
    }

    async componentDidMount() {
       
    }


    _register = async () => {
        const salonObject = {
            firstName : this.state.firstName,
            lastName : this.state.lastName,
            email: this.state.email,
            contactNumber:this.state.contactNumber,
            userType: 'StoreOwner',
            status : 'Pending',
            address: {
                line1: this.state.address,
                line2: this.state.address2,
                city: this.state.city,
                postal_code: this.state.postal_code
            },
            store : [{
                storeName: this.state.storeName,
            }],
        }
         await console.log(salonObject)

        await api.post('/user', salonObject)
            .then(response => {
                console.log('Adding Store Owner Result');
                console.log(response.data);
                this.setState({isComplete: true});
        }).catch(error => { 
            this.setState({isComplete: false});
            console.log(error) 
        })
    }


    

 
    render() {
        return (
            <View style={{ alignItems: 'center', justifyContent: 'center', width: DEVICE_WIDTH, height: DEVICE_HEIGHT, borderBottomWidth: wp('1.8%'), borderBottomColor: "#333333", alignItems: 'stretch' }}>
                <View style={{ height: hp('15%'), flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingTop: hp('4.6%'), borderBottomColor: "#333333",backgroundColor: '#634262'}}>
                    <Text style={[styles.heading, { textAlign: 'center', alignItems: 'center', justifyContent: 'center' }]}>Do You Have Salon?</Text>
                </View>
                <ScrollView>
                <KeyboardAvoidingView behavior={"padding"} style={styles.container} enabled keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 100}>
                
                    <View style={{padding:30}}>
                        <View style={{marginBottom:30}}>
                            <Text style={{ fontSize: 17, color: '#885F88',marginBottom:10,textAlign:'center'}}>Welcome to GoAwra</Text>
                            <Text style={{ fontSize: 15, color: '#885F88',textAlign:'center'}}>We would love to have onboard with us. Would you please fill the below information</Text>
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="First Name" 
                                placeholderTextColor="#727272"
                                onChangeText={firstName => this.setState({firstName})}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="Last Name" 
                                placeholderTextColor="#727272"
                                onChangeText={lastName => this.setState({lastName})}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="Salon Name" 
                                placeholderTextColor="#727272"
                                onChangeText={storeName => this.setState({storeName})}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="Contact Number" 
                                placeholderTextColor="#727272"
                                keyboardType="phone-pad"
                                onChangeText={contactNumber => this.setState({contactNumber})}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="Email" 
                                placeholderTextColor="#727272"
                                keyboardType="email-address"
                                onChangeText={email => this.setState({email})}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="Address Line 1" 
                                placeholderTextColor="#727272"
                                keyboardType="phone-pad"
                                onChangeText={address => this.setState({address})}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="Address Line 2" 
                                placeholderTextColor="#727272"
                                onChangeText={address2 => this.setState({address2})}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="City" 
                                placeholderTextColor="#727272"
                                onChangeText={city => this.setState({city})}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="Postal Code" 
                                placeholderTextColor="#727272"
                                onChangeText={postal_code => this.setState({postal_code})}
                            />
                        </View>
                        <TouchableOpacity onPress={() => { this._register() }} style={styles.loginBtn}>
                            <Text style={styles.loginText}>REGISTER NOW</Text>
                        </TouchableOpacity>
                        
                        </View>

                </KeyboardAvoidingView>
                </ScrollView>
                <RegistrationComplete status={this.state.isComplete} navigation={this.props.navigation}/>
            </View>
            
            
        )
    }
}

export default SalonRegistrationScreen;