import { Ionicons } from '@expo/vector-icons';
import * as React from 'react';
import { StyleSheet, View, Dimensions, Image, ImageBackground, Button,TouchableOpacity } from 'react-native';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import {FilledButton} from '../components/FilledButton';
import Text from '../components/CustomText';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

export default function LoginSelectionScreen({navigation}) {
  return (
    <View style={ styles.container }>
      <ImageBackground source={require('../assets/images/login-background.jpg')} style={styles.backgroundImage}>
            <View style={styles.contentContainer}>
              <View  style={styles.logoContainer}>
              <Image
                  source={require('../assets/images/GoAwra-logo.png')}
                  style={{ width: 208, height: 185 }}
                  resizeMode="contain"
              />
              </View>

              <View style={{alignItems: 'center'}}>
                <Text type="bold" style={{ fontSize: 27,color: '#885F88',lineHeight: 33,letterSpacing: 3,marginBottom:30 }}>WELCOME</Text>
                <Text style={{ fontSize: 14,color: '#885F88',lineHeight: 18,letterSpacing: 2,marginBottom: 60}}>SIGN UP AS</Text>
              </View>

              <View style={styles.buttonContainer}>
                <FilledButton
                  title={'CUSTOMER'}
                  style={styles.button}
                  onPress={
                    async () => {
                      navigation.navigate('CustomerLogin');
                    }
                  }
                />
                <FilledButton
                  title={'BEAUTY THERAPIST'}
                  style={styles.button}
                  onPress={async () => {
                    navigation.navigate('TherapistOnBoardingStep1');
                  }}
                />
              </View>
              <TouchableOpacity
                    onPress={
                      async () => {
                        navigation.navigate('SalonRegistration');
                      }
                    }>
                  <Text type="bold" style={{ fontSize: 14, color: '#885F88',lineHeight: 18}}>or Do you have salon?</Text>
              </TouchableOpacity>
              <View style={{}}>
                
              </View>
            </View>
          </ImageBackground>
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: DEVICE_WIDTH,
    height: DEVICE_HEIGHT,
    alignItems:'center',
    justifyContent:'center',
  },
  buttonContainer: {
    
    // alignItems: 'center',
    // justifyContent: 'center',
    flexDirection: 'row',
    textAlign: 'center',
    marginBottom: 60,
    

  },
  logoContainer: {
    marginBottom: 40,
    alignItems: 'center' ,marginTop: (DEVICE_HEIGHT / 2) - hp('31%')
  },
  button : {
    height: 65,
    backgroundColor: '#A375A4',
    width: (DEVICE_WIDTH / 2) - wp('5%'),
    margin:5,
    justifyContent:'center',alignItems:'center',
    letterSpacing: 100,
    // justifyContent: 'flex-end',
    // flex: 1,

  },
  contentContainer: {
    // flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // width: DEVICE_WIDTH - 50, 
    height: DEVICE_HEIGHT,
    // width: DEVICE_WIDTH - wp('20%'), 
  },
  backgroundImage: {
      flex: 1,
      resizeMode: 'cover', // or 'stretch'
      // height: hp('110%'),
      position: 'absolute', top: 0, bottom: 0, left: 0, right: 0
  },
});
