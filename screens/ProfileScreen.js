import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';


export default class ProfileScreen extends Component {

  static navigationOptions = {
     headerShown: false,
      tabBarVisible: false,
  };
  render() {
    return (
      <View style={styles.MainContainer}>
        <Text style={{ fontSize: 23 }}> Profile Screen </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    paddingTop: 20,
    alignItems: 'center',
    marginTop: 50,
    justifyContent: 'center',
  },
});