
import React, { Component } from 'react';
import { StyleSheet, View, ScrollView,SafeAreaView,FlatList,TouchableOpacity,TouchableHighlight,Dimensions,ActivityIndicator } from 'react-native';
import { ListItem, Avatar, Card, Image } from 'react-native-elements';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import AppHeader from '../components/AppHeader';
import Text from '../components/CustomText';
import Filter from '../components/Filter';
import api from '../utils/api';

const DEVICE_WIDTH = Dimensions.get('window').width;
const CONTAINER_WIDTH = DEVICE_WIDTH / 2 - 8;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const CONTAINER_HEIGHT = DEVICE_HEIGHT / 2 - 8;

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
  },
];
const Item = ({ title }) => (
  <View style={styles.item}>
    <Text style={styles.title}>{title}</Text>
  </View>
);

export default class AvailableBeauticiansScreen extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      beauticians: [
        // {
        //   id:1,
        //   avatar: require('../assets/images/pedroAcre.png'),
        //   FullName: 'Pedro Acre'
        // },
        // {
        //   id:2,
        //   avatar: require('../assets/images/pedroAcre.png'),
        //   FullName: 'Angel Lockring'
        // },
        // {
        //   id:3,
        //   avatar: require('../assets/images/pedroAcre.png'),
        //   FullName: 'Acre Pedro'
        // },
        // {
        //   id:4,
        //   avatar: require('../assets/images/pedroAcre.png'),
        //   FullName: 'Lockring Angel'
        // },
        // {
        //   id:5,
        //   avatar: require('../assets/images/pedroAcre.png'),
        //   FullName: 'Pedro Acre'
        // },

      ],
    }
    this.onPress = this.onPress.bind(this);
    
  }

  async componentDidMount() {
    await this.setState({ isLoading: true })
    await api.get('/user/service/beauticians/'+this.props.navigation.getParam('service_id'))
    .then(async (response) =>  {
        if (response.data.beauticians){
            this.setState({ 'beauticians': response.data.beauticians });
            await this.setState({ isLoading: false })
            console.log('DATA---------- LOADED');
            console.log('loading----------'+this.state.isLoading);
            console.log(response.data.beauticians);
        } else {
            await this.setState({ isLoading: true })
            // console.log('loading----------'+this.state.isLoading);
        }
        // console.log(this.state.beauticians[0]);
        
    }).catch((error)=>{
        console.log(error.response);
        alert(error.message);
     });

    console.log('service ID:  '+JSON.stringify(this.props.navigation.getParam('service_id')))
}

  onPress(val) {
    console.log(val);
  }
  _isEven(value) {
    if (value%2 == 0)
      return true;
    else
      return false;
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Beautician - '+ navigation.getParam('service'),
    };
  };

  // static navigationOptions = ({ navigation }) => ({
  //   // header: <AppHeader showBadge={true} leftAction={() => { return null }} leftLabel={' '} rightAction={() => navigation.navigate('ViewCart')} rightLabel={<Ionicons name={Platform.OS === 'ios' ? 'ios-cart' : 'md-cart'} size={25} />} />
  //   header: <AppHeader/>
  // });
  render() {

    const { navigation } = this.props;
  
    return (
      
      <View style={styles.container}>
        <ScrollView contentContainerStyle={{paddingBottom:10}}>
        <View style={{flex: 1, flexDirection: 'row', width: DEVICE_WIDTH}}>
          <View style={[styles.viewStyle, {flex: 1, flexDirection: 'row'}]}>
            {!this.state.isLoading ? (
              this.state.beauticians  && this.state.beauticians.length ? (
                this.state.beauticians.map((beautician,index) => {
                  // console.log(beautician);
                  return (
                    <TouchableOpacity key={index}
                      onPress={() => {
                        this.props.navigation.navigate('BookingBeautician', {
                          beautician: '',
                        });
                      }}
                      style={styles.childStyle}
                    >
                        <Image style={{justifyContent: 'center',
                          borderTopLeftRadius:12,
                          borderTopRightRadius:12,
                          height:200
                        }}
                          // source={require('../assets/images/pedroAcre.png')}
                          source={(beautician.photoUrl) ? {uri:beautician.photoUrl} : require('../assets/images/pedroAcre.png')}
                        />
                        <View style={styles.nameContainer}>
                          <Text style={styles.name}>
                            {beautician.FullName}
                          </Text>
                        </View>
                    </TouchableOpacity>
                  )
                })
              ) : (
                <View style={styles.gifContainer}>
                    <Text>No Available Beautician</Text>
                </View>
              )
            ) : (
              <View style={styles.gifContainer}>
                  <ActivityIndicator size="large" color="#644263" />
              </View>
            )}
       
            
          </View>
          </View>
          </ScrollView>
        </View>

        

      
      
    );
  }
}

const styles = StyleSheet.create({
  gifContainer: {
    flex: 1,
    marginTop:20,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
},
  container: {
    flex: 1,
    // flexDirection:'row',
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  column: {
    flexDirection:'column',
    justifyContent:'space-between',
    alignItems:'center',
    // height:200,
    width:"50%"
  },
  space_between_columns:{
    width:15
  },
  box: {
    height:50,
    // width:"100%",
    width:"100%",
    backgroundColor: 'green',
  },
  nameContainer: {
    // position: 'absolute',
    padding:10,
    backgroundColor: '#644263',
    width:"100%",
    borderBottomLeftRadius:12,borderBottomRightRadius:12
  },
    name: {
    // position: 'absolute',
    fontWeight: '400',
    fontSize:18,
    bottom:0,
    color:'#fff',
    textTransform:'uppercase',
    textAlign:'center',
  },
  viewStyle: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    padding: 10,

    justifyContent: 'space-between'
  },
  childStyle: {
    width: '49%',
    marginBottom: 10,
    // marginBottom: space
  }
});

// const styles = StyleSheet.create({
//     container: {
//         flex:1,
//         backgroundColor: '#FFFFFF',
//         width: DEVICE_WIDTH,
//     },
//     imageContainer:{
//         // width: CONTAINER_WIDTH,
//         // marginHorizontal: 5,
//         marginTop: hp('0.5%'),
//     },
//     name: {
//     // position: 'absolute',
//     fontWeight: '400',
//     fontSize:20,
//     bottom:0,
//     color:'#FFFFFF',
//     backgroundColor: '#644263',
//     width:"100%",
//     textAlign:'center',

//     borderBottomRightRadius: 10,
//     borderBottomLeftRadius: 10,
//     overflow: 'hidden',
//     padding:5,
//   },
 
//   row: {
//     flex: 1,
//     justifyContent: "space-around"
//   },
//   borderRadius:{
//     borderRadius: (DEVICE_HEIGHT * 0.03)/2
//   },
//   card: {
//     borderRadius: 8,
//     borderWidth: 0, // Remove Border
//     elevation:0,
//     shadowColor: 'rgba(0,0,0, .2)',
//     shadowOffset: { height: 0, width: 0 },
//     shadowOpacity: 0, //default is 1
//     shadowRadius: 0//default is 1
//   },
//   containertest: {
//     flex: 1,
//     flexDirection: 'row',
//     flexWrap: 'wrap',
//     alignItems: 'flex-start', // if you want to fill rows left to right
    
    
//   },
//   itemtest: {

//     width: '48%' // is 50% of container width
//   }
// });