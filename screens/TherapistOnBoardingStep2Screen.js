
import React, { Component } from 'react';
import {
    View,
    TextInput,
    Dimensions,
    Image,
    StyleSheet,
    TouchableOpacity,
    FlatList,
    Button,
    ActivityIndicator,
    ScrollView
} from 'react-native';

import Text from '../components/CustomText';
import BulletNavigation from '../components/BulletNavigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ServiceListItem from '../components/ServiceListItem';
import TagInput from 'react-native-tags-input';
import api from '../utils/api';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const inputTagColor = '#644263';

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    paragraph: {
        marginBottom: hp('3.5%'),
        textAlign: 'center'
    },
    heading: {
        fontSize: 24,
        color: 'white'
    },
    gifContainer: {
        flex: 1,
        marginTop:20,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textInputTag: {
        marginTop: 5,
        padding: 5,
    },
    tag: {
        backgroundColor: '#E6E6E6',
        borderRadius: 5,
        height: 45,
        paddingRight:5,
        marginTop: 0
    },
    tagText: {
        color: '#644263'
    },
    
});

class TherapistOnBoardingStep2Screen extends Component {
    constructor(props) {
        super();

        this.state = {
            selectedServices: [],
            disableNextButton: true,
            services: [],
            isLoading: false,
            otherSkills: [],
            tags: {
                tag: '',
                tagsArray: []
            },
            params : {
                tags: {
                    tag: '',
                    tagsArray: []
                },
                selectedServices: [],
            }
        }
        this.handleClick = this.handleClick.bind(this);
    }

    async componentDidMount() {
        await this.setState({ isLoading: !this.state.isLoading })

        await api.get('/services')
        .then(async (response) =>  {
            if (response.data.services){
                this.setState({ 'services': response.data.services });
                await this.setState({ isLoading: this.state.isLoading })
            } else {
                await this.setState({ isLoading: !this.state.isLoading })
            }
        }).catch((error)=>{
            console.log(error.response);
            alert(error.message);
         });
    }

    disableButton = (status) => {
        this.setState( prevState => ({
            disableNextButton: status
        }));
    }
    selectService = async (id) => {

        if (this.state.params.selectedServices.find(service => service === id)) {
            const service = this.state.params.selectedServices.filter(item => item !== id);
            this.setState(prevState => ({
                params: {                   
                    ...prevState.params,    
                    selectedServices: service     
                }
            }), () => {
                if (this.state.params.selectedServices.length >= 3) {
                    this.disableButton(false);
                } else {
                    this.disableButton(true);
                }
            });
        }
        else {
            this.setState(prevState => ({
                params: {                   
                    ...prevState.params,    
                    selectedServices: [...prevState.params.selectedServices, id],      
                }
            }), () => {
                if (this.state.params.selectedServices.length >= 3) {
                    this.disableButton(false);
                } else {
                    this.disableButton(true);
                }
            });
        }
    }
    skills = async () => {
        
    }
    updateTagState = (state) => {
        this.setState( prevState => ({
                params: {                   
                    ...prevState.params,    
                    tags: state
                }
        }))
        console.log(this.state.params)
    };
    handleClick = async () => {
        console.log('click next.')
        // console.log(this.state.tags)
    }
    
    
    render() {
        // console.log(this.state.otherSkills)
        return (
            <View style={{ alignItems: 'center', justifyContent: 'center', width: DEVICE_WIDTH, height: DEVICE_HEIGHT, borderBottomWidth: wp('1.8%'), borderBottomColor: "#333333", alignItems: 'stretch' }}>
                <View style={{ height: hp('15%'), flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingTop: hp('4.6%'), borderBottomColor: "#333333",backgroundColor: '#634262'}}>
                    <Text style={[styles.heading, { textAlign: 'center', alignItems: 'center', justifyContent: 'center' }]}>Share us your skills</Text>
                </View>
                <View style={{flex: 1,paddingBottom:20}}>
                    <ScrollView contentContainerStyle={{paddingBottom:50}}>
                        {!this.state.isLoading ? (
                            this.state.services ? (
                                this.state.services.map((service,index) => {
                                    return (
                                        <View key={index}>
                                            <ServiceListItem 
                                                name={service.category}
                                                id={service.id}
                                                selectService={() => { this.selectService(service.id) }}
                                            />
                                            {(index == this.state.services.length-1)? (
                                                <TagInput 
        
                                                updateState={this.updateTagState}
                                                tags={this.state.params.tags}
                                                placeholder="Other Skills:"                            
                                                // label='Press comma & space to add a tag'
                                                labelStyle={{color: '#E6E6E6'}}
                                                // leftElement={<Icon name={'tag-multiple'} type={'material-community'} color={this.state.tagsText}/>}
                                                leftElementContainerStyle={{marginLeft: 3}}
                                                // containerStyle={{width: (Dimensions.get('window').width - 40)}}
                                                inputContainerStyle={styles.textInputTag}
                                                // inputStyle={{color: this.state.tagsText}}
                                                // onFocus={() => this.setState({tagsColor: '#fff', tagsText: mainColor})}
                                                // onBlur={() => this.setState({tagsColor: mainColor, tagsText: '#fff'})}
                                                autoCorrect={false}
                                                tagStyle={styles.tag}
                                                // tagTextStyle={styles.tagText}
                                                keysForTag={', '}/>
                                            ): (<View/>)}
                                        </View>

                                    );
                                    
                                })
                            ) : (
                                <View style={styles.gifContainer}>
                                    <Text>No existing Services</Text>
                                </View>
                                )
                        ) : (
                            <View style={styles.gifContainer}>
                                <ActivityIndicator size="large" color="#644263" />
                            </View>
                        )}
                        
                    </ScrollView>
                </View>
                <View>
                    <BulletNavigation
                        next="TherapistOnBoardingStep3"
                        max="3"
                        active="2"
                        navigation={this.props.navigation}
                        disable={this.state.disableNextButton}
                        params={this.state.params}
                        back="true"
                    />
                </View>
                {/* <Loading loading={this.state.isLoading} /> */}
                
            </View>
            
        )
    }
}

export default TherapistOnBoardingStep2Screen;