
import React, { Component } from 'react'
import {
    Platform,
    View,
    TextInput,
    Dimensions,
    Image,
    StyleSheet,
    TouchableOpacity,
    FlatList,
    Button,
    ActivityIndicator,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import RNPickerSelect, { defaultStyles } from 'react-native-picker-select';

import { MaterialCommunityIcons } from '@expo/vector-icons';

import Text from '../components/CustomText';

// import { connect } from "react-redux";
import BulletNavigation from '../components/BulletNavigation';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import api from '../utils/api';

// import {Loading} from '../components/Loading';
import {RegistrationComplete} from '../components/RegistrationComplete';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
    //   fontSize: 16,
    // paddingVertical: 1,
    //    paddingHorizontal: 10,
    //   color: 'black',
    //   paddingRight: 10, // to ensure the text is never behind the icon
    },
    inputAndroid: {
    //   fontSize: 16,
    //   paddingHorizontal: 10,
    //   paddingVertical: 8,
    //   color: 'black',
    //   paddingRight: 10, // to ensure the text is never behind the icon
    },
  });

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    inputView:{
        backgroundColor:"#E5E5E5",
        borderRadius:5,
        marginBottom:10,
        justifyContent: "center",
        padding:15,
        height:(Platform.OS === "ios" ? 'auto': 50)
        
      },
      inputViewAndroid:{
        backgroundColor:"#E5E5E5",
        borderRadius:5,
        marginBottom:10,
        justifyContent: "center",
        padding:15,
        paddingLeft:5,
        height:(Platform.OS === "ios" ? 'auto': 50)
        
      },
      inputText:{
        // height:50,
        fontSize:15,
        color: '#000',
      },
      loginBtn:{
        width:"100%",
        backgroundColor:"#A375A3",
        fontSize:18,
        borderRadius:5,
        height:50,
        alignItems:"center",
        justifyContent:"center",
        marginTop:10,
        marginBottom:10
      },
      loginText: {
        color: 'white',
        letterSpacing: 2,
      },
    paragraph: {
        marginBottom: hp('3.5%'),
        textAlign: 'center'
    },
    heading: {
        fontSize: 24,
        color: 'white'
    },
    gifContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
});

const gender = [
    {
      label: 'Male',
      value: 'Male',
    },
    {
      label: 'Female',
      value: 'Female',
    }
];


class TherapistOnBoardingStep3Screen extends Component {
    constructor(props) {
        super();

        this.state = {
            // selectedServices: (this.props.route.params.selectedServices) ? this.props.route.params.selectedServices: '',
            disableNextButton: true,
            selectedServices: [],
            isComplete: false,
            firstName: '',
            lastName: '',
            age: '',
            gender: '',
            contactNumber: '',
            email: '',
            address: '',
            address2: '',
            city: '',
            postal_code: '',
            params: ''


        }
    }

    async componentDidMount() {
        this.setState({params: this.props.navigation.getParam('params')})
    }

    _register = async () => {        
        const therapistObject = {
                firstName : this.state.firstName,
                lastName : this.state.lastName,
                age : this.state.age,
                gender : this.state.gender,
                email: this.state.email,
                contactNumber:this.state.contactNumber,
                userType: 'Beautician',
                address: {
                    line1: this.state.address,
                    line2: this.state.address2,
                    city: this.state.city,
                    postal_code: this.state.postal_code
                },
                skills: this.state.params.tags.tagsArray,
                operateServices : (this.state.params.selectedServices) ? this.state.params.selectedServices : '',
        }

        await api.post('/user', therapistObject)
            .then(response => {
                console.log('Adding Beautician Result');
                console.log(response);
                this.setState({isComplete: true});
        }).catch(error => { 
            this.setState({isComplete: false});
            console.log(error) 
        })
    }

    render() {
        // console.log(this.state)
        return (
            <View style={{ alignItems: 'center', justifyContent: 'center', width: DEVICE_WIDTH, height: DEVICE_HEIGHT, borderBottomWidth: wp('1.8%'), borderBottomColor: "#333333", alignItems: 'stretch' }}>
                <View style={{ height: hp('15%'), flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingTop: hp('4.6%'), borderBottomColor: "#333333",backgroundColor: '#634262'}}>
                    <Text style={[styles.heading, { textAlign: 'center', alignItems: 'center', justifyContent: 'center' }]}>Registration Page</Text>
                </View>
                <KeyboardAvoidingView behavior={"padding"} style={styles.container} enabled keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 100}>
                <ScrollView>
                    <View style={{padding:30}}>
                    
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="First Name" 
                                placeholderTextColor="#727272"
                                onChangeText={firstName => this.setState({firstName})}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="Last Name" 
                                placeholderTextColor="#727272"
                                onChangeText={lastName => this.setState({lastName})}
                            />
                        </View>
                        
                        <View style={(Platform.OS === "ios" ? styles.inputView: styles.inputViewAndroid)}>
                            <RNPickerSelect
                                placeholder={{
                                    label: 'Gender',
                                    value: null,
                                    color: '#727272',
                                }}
                                items={gender}
                                onValueChange={gender => {this.setState({gender});}}
                                style={{
                                    placeholder: {
                                        color: '#727272',
                                    },
                                    justifyContent: "center",

                                }}
                                // useNativeAndroidPickerStyle={false}
                                // textInputProps={{ underlineColor: 'yellow' }}
                                Icon={() => {
                                    // (Platform.OS === "ios" ? 'auto': 50)
                                    return <MaterialCommunityIcons style= {{marginTop:(Platform.OS === "ios" ? 'auto': 15)}} name="chevron-down" size={20} color="gray" />;
                                }}
                            />
                        </View>
                        
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="Age" 
                                placeholderTextColor="#727272"
                                keyboardType="number-pad"
                                maxLength={2}
                                onChangeText={age => this.setState({age})}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="Email" 
                                placeholderTextColor="#727272"
                                keyboardType="email-address"
                                onChangeText={email => this.setState({email})}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="Contact Number" 
                                placeholderTextColor="#727272"
                                keyboardType="phone-pad"
                                onChangeText={contactNumber => this.setState({contactNumber})}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="Address Line 1" 
                                placeholderTextColor="#727272"
                                keyboardType="phone-pad"
                                onChangeText={address => this.setState({address})}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="Address Line 2" 
                                placeholderTextColor="#727272"
                                onChangeText={address2 => this.setState({address2})}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="City" 
                                placeholderTextColor="#727272"
                                onChangeText={city => this.setState({city})}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput  
                                style={styles.inputText}
                                placeholder="Postal Code" 
                                placeholderTextColor="#727272"
                                onChangeText={postal_code => this.setState({postal_code})}
                            />
                        </View>

                        {/* <View style={{}}> */}
                        <TouchableOpacity onPress={() => { this._register() }} style={styles.loginBtn}>
                            <Text style={styles.loginText}>SUBMIT</Text>
                        </TouchableOpacity>
                        {/* </View> */}
                        
                        
                    </View>
                    </ScrollView>
                </KeyboardAvoidingView>
                

                <View>
                    <BulletNavigation max="3" active="3" back="true" navigation={this.props.navigation}/>
                </View>
                <RegistrationComplete status={this.state.isComplete} navigation={this.props.navigation}/>
                
            </View>
            
        )
    }
}

export default TherapistOnBoardingStep3Screen;