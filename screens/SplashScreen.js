import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    Dimensions,
    StyleSheet,
    StatusBar,
    Image,
    ImageBackground
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
// import { useTheme } from '@react-navigation/native';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;


export default class SplashScreen extends Component {
    constructor(props) {
        super()
        this.state = {
            // screen: 'Dashboard'
            screen: 'LoginSelection'
        }
    }
    
    
    
    async componentDidMount(){
        // await AsyncStorage.removeItem('@userData')
        let userData = await this._retrieveData('@userData');
        let screen = this.state.screen;
        if(userData){
            screen = 'Dashboard';
        }else{
            screen = 'Auth';
        }
         screen = "Wallet";
        this.setState({ screen })
    }

    _retrieveData = async (key) => {
        try {
            const userData = await AsyncStorage.getItem(key);
            if (userData !== null) {
                return userData
            }
            else {
                return false;
            }
        } catch (error) {
            // Error retrieving data
        }
    };
    
    render() {
        setTimeout(() => {
            this.props.navigation.navigate(this.state.screen);
        }, 1000);

        return (
            <View style={styles.container}>
                    <ImageBackground source={require('../assets/images/bg-color.jpg')} style={styles.backgroundImage}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', alignContent:'center', width: DEVICE_WIDTH, height: DEVICE_HEIGHT}}>
                            <ImageBackground source={require('../assets/images/tail.png')} style={{width: 289, height: 590,justifyContent: 'center', alignItems: 'center', alignContent:'center'}}>
                                <Image
                                    source={require('../assets/images/GoAwra-logo.png')}
                                    // style={{ width: 208, height: 185 }}
                                    style={styles.logo}
                                />
                            </ImageBackground> 
                        </View>
                    </ImageBackground>
            </View>
        );
    } 
}


const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor: '#644263'
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
},
  header: {
      flex: 2,
      justifyContent: 'center',
      alignItems: 'center'
  },
  footer: {
      flex: 1,
      backgroundColor: '#fff',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      paddingVertical: 50,
      paddingHorizontal: 30
  },
  logo: {
      width: 208,
      height: 185,
      flex: 1,
    // alignItems: "center",
    // // resizeMode: "cover",
    // justifyContent: "center"
    
    resizeMode:'contain'
  },
  title: {
      color: '#05375a',
      fontSize: 30,
      fontWeight: 'bold'
  },
  text: {
      color: 'grey',
      marginTop:5
  },
  button: {
      alignItems: 'flex-end',
      marginTop: 30
  },
  signIn: {
      width: 150,
      height: 40,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 50,
      flexDirection: 'row'
  },
  textSign: {
      color: 'white',
      fontWeight: 'bold'
  }
});

