import React, { Component } from 'react';
import { StyleSheet, View, ScrollView,SafeAreaView,FlatList,TouchableOpacity,TouchableHighlight } from 'react-native';
import { ListItem, Avatar, Card, Image } from 'react-native-elements';

import AppHeader from '../components/AppHeader';
import Text from '../components/CustomText';
import api from '../utils/api';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
  },
];
const Item = ({ title }) => (
  <View style={styles.item}>
    <Text style={styles.title}>{title}</Text>
  </View>
);

export default class NotificationScreen extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      list: [
        {
          name: 'Amy Farha',
          avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
          subtitle: 'Vice President'
        },
        {
          name: 'Chris Jackson',
          avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
          subtitle: 'Vice Chairman'
        },
        {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },

      ]
    }
    
  }



  async componentDidMount() {
    // await this.setState({ isLoading: true })
    // await api.get('/packages').then(async (response) =>  {
    //   if (response.data.packages && response.data.packages.status != 'Active'){
    //       this.setState({ 'packages': response.data.packages });
    //       // await this.setState({ isLoading: false })
    //       // console.log('DATA---------- LOADED');
    //       // console.log('loading----------'+this.state.isLoading);
    //       console.log(response.data.packages);
    //   } else {
    //       await this.setState({ isLoading: true })
    //       // console.log('loading----------'+this.state.isLoading);
    //   }
    //     // console.log(this.state.beauticians[0]);
        
    // }).catch((error)=>{
    //     console.log(error.response);
    //     alert(error.message);
    //  });
    console.log(this.state.list)

    // console.log('service ID:  '+JSON.stringify(this.props.navigation.getParam('service_id')))
}
    keyExtractor = (item , index) => index.toString();
    renderItem = ({ item }) => {
        <ListItem 
            title={item.name}
            // subtitle={'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'}
            bottomDivider={true}
        />
    }

  render() {
        return(
            <SafeAreaView style={styles.container}>
                <FlatList 
                    data={this.state.list}
                    // renderItem={this.renderItem}
                    renderItem={({ item, index }) => {
                        let evenRow = index.toString() % 2 == 0;
                        console.log(evenRow)
                        return(
                            <View style={[styles.listView, evenRow && styles.evenColor]}>
                                <View style={styles.title}>
                                    <View style={styles.titleName}>
                                        <Text style={{fontSize:20}}>{item.name}</Text> 
                                    </View>
                                    <View style={styles.date}>
                                        <Text>06/06/20</Text>
                                    </View>
                                </View>
                                <View style={styles.content}>
                                    <Text style={{fontSize:14}}>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </Text>
                                </View>
                            </View>
                            // <ListItem style={{backgroundColor:'#000'}} bottomDivider>
                            // {/* <ListItem style={[styles.listItem, evenRow && styles.evenColor]} bottomDivider> */}
                            //     <ListItem.Content>
                                    // <View style={styles.title}>
                                    //     <View style={styles.titleName}>
                                    //         <Text style={{fontSize:20}}>{item.name}</Text> 
                                    //     </View>
                                    //    <View style={styles.date}>
                                    //         <Text>06/06/20</Text>
                                    //    </View>
                                    // </View>
                                    // <View style={styles.content}>
                                    //     <Text style={{fontSize:14}}>
                                    //         Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    //     </Text>
                                    // </View>
                            //     </ListItem.Content>
                            // </ListItem>
                            
                            // <ListItem 
                            //     title={item.name}
                            //     // subtitle={'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'}
                            //     bottomDivider={true}          
                            // />
                        )
                    }}
                    keyExtractor={this.keyExtractor}
                />
            </SafeAreaView>
            
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  listView:{
    padding:20,
    backgroundColor:'#fff',
  },
  listItem:{
    // flexDirection: 'row',
    backgroundColor:'#fff',
    
  },
  evenColor: {
    backgroundColor: '#F5F5F5'
    // backgroundColor: '#000'
  },
  content: {
    flexDirection: 'row',
    paddingTop: 5
  },
  date: {
    textAlign: 'right',
    fontSize:14
  },
  title:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    
  },
  titleName:{
    textAlign: 'left',
    fontSize:16
  }
});