import { Ionicons } from '@expo/vector-icons';
import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, Image, ImageBackground, Button,TextInput,TouchableOpacity,Modal,Alert } from 'react-native';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import CONFIG from '../config/config';
import * as Facebook from 'expo-facebook';
import * as Google from 'expo-google-app-auth';
import * as Constants from 'expo-constants';
import AsyncStorage from '@react-native-community/async-storage';

import Text from '../components/CustomText';
import {Loading} from '../components/Loading';
import api from '../utils/api';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;


const toDataUrl = (url, callback) => {
  const xhr = new XMLHttpRequest();
  xhr.onload = () => {
      const reader = new FileReader();
      reader.onloadend = () => {
          callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
  };
  xhr.open('GET', url);
  xhr.responseType = 'blob';
  xhr.send();
};

async function showAlert(title, message, onPress = null) {
  await Alert.alert(
    title,
    message,
    [
      {
        text: 'OK',
        onPress: onPress == null ? null : () => { onPress(); },
      },
    ],
    { cancelable: false },
  );
}



export default class CustomerLoginScreen extends Component {
  constructor(props) {
      super()
      this.state = {
        modalVisible:false,
        loading: false,
        email: '',
        password: '',
        photoUrl: '',


      }
  }
  _socialLogin = async (url,obj) => {
    await api.post(url,obj)
    .then(async (response) =>  {
        const responseBody = response.data.data;
        responseBody.user.photoUrl = obj.user.photoUrl;
        await AsyncStorage.setItem('@userData', JSON.stringify(responseBody.user))
        // return true;
        const userData = await AsyncStorage.getItem('@userData')
        if(userData !== null) {
            await this.props.navigation.navigate('Dashboard');
        }else{
            //return false;
            alert('Something is wrong! Please contact Administrator. \n Result: ' + userData);
        }
    })
    .catch(error => {
        alert(error); // Network Error
    });
  }


  login = async (provider,user) => {
    
    switch (provider) {
      case 'facebook':
        // try {
        //   await Facebook.initializeAsync(CONFIG.FACEBOOK_APP_ID);
        //   const {
        //     type,
        //     token,
        //     expires,
        //     permissions,
        //     declinedPermissions,
        //   } = await Facebook.logInWithReadPermissionsAsync({
        //     permissions: ['public_profile','email'],
        //   });
        //   if (type === 'success') {
        //     // Get the user's name using Facebook's Graph API
        //     const response = await fetch(`https://graph.facebook.com/me?fields=id,name,email,picture.type(large)&redirect=0&access_token=${token}`);
            
        //     // Alert.alert('Logged in!', `Hi ${(await response.json()).name}!`);
        //     let user = await response.json();
        //     toDataUrl(user.picture.data.url, (myBase64) => {
        //       console.log(myBase64); // myBase64 is the base64 string
        //     });
        //     console.log(user);
        //   } else {
        //     // type === 'cancel'
        //   }
        // } catch ({ message }) {
        //   alert(`Facebook Login Error: ${message}`);
        // }


        try {
          await Facebook.initializeAsync(CONFIG.FACEBOOK_APP_ID);
          const {
              type,
              token,
              expires,
              permissions,
              declinedPermissions,
          } = await Facebook.logInWithReadPermissionsAsync({
              permissions: ['public_profile','email'],
            });
          if (type === 'success') {
              // Get the user's name using Facebook's Graph API
              // console.log('login worked');
              const response = await fetch(`https://graph.facebook.com/me?fields=id,name,email,picture.type(large)&redirect=0&access_token=${token}`);
              let user = await response.json();
        
                var name = user.name.split(" ");
                let firstName = name[0];
                let lastName = (name[1]) ? name[1] : '';

                // toDataUrl(user.picture.data.url, (myBase64) => {
                //         console.log(myBase64); // myBase64 is the base64 string
                //       });
                // toDataUrl(user.picture.data.url,(myBase64) => {
                //   // this.setState({photoUrl:myBase64})
                //   // console.log(myBase64);
                //   var photoUrl = myBase64;
                // });
                let photoUrl = user.picture.data.url;
                let obj = {
                    provider: 'facebook',
                    token: token,
                    user: {
                        id: user.id,
                        firstName: firstName,
                        lastName: lastName,
                        email: user.email,
                        photoUrl: photoUrl
                    }
                }
                console.log(obj)
                await this._socialLogin('/auth/login', obj)
              } else {
                  // type === 'cancel'
                  console.log('cancelled');
              }
        } catch ({ error }) {
              alert(`Facebook Login Error: ${error}`);
        }

      break;
      case 'google':
        try {
          const { type, accessToken, user } = await Google.logInAsync({
              expoClientId: CONFIG.google_expoClientId,
              iosClientId: CONFIG.google_iosClientId,
              androidClientId: CONFIG.google_androidClientId,
              iosStandaloneAppClientId: CONFIG.google_iosClientId,
              androidStandaloneAppClientId: CONFIG.google_androidClientId,
          });
    
          if (type === 'success') {
              let obj = {
                  provider: 'google',
                  token: accessToken,
                  user: {
                      id: user.id,
                      firstName: user.givenName,
                      lastName: user.familyName,
                      email: user.email,
                      photoUrl: user.photoUrl
                  }
              }
              // console.log(obj)
               await this._socialLogin('/auth/login', obj)
          }else {
            // type === 'cancel'
            console.log('cancelled');
          }
        } catch (error) {
          alert(`Google Login Error: ${error}`);
        }
      break;
      default:
        //local
        // console.log(user.email);
        // console.log(user.password);
        
        let obj = {
          provider: 'local',
          user
      }
      // console.log(obj)
      await this._socialLogin('/auth/login', obj)
    }
  }



  render() {
   
  return (
    <View style={ styles.container }>
      <ImageBackground source={require('../assets/images/login-background.jpg')} style={styles.backgroundImage}>
            <View style={styles.contentContainer}>
              <View  style={styles.logoContainer}>
                <Image
                    source={require('../assets/images/GoAwra-logo.png')}
                    style={{ width: 208, height: 185 }}
                    resizeMode="contain"
                />
                {/* <Image  style={{ width: 208, height: 185 }}
                source={(this.state.photoUrl) ? {uri:this.state.photoUrl} : require('../assets/images/avatar.png')}
                size={100}
              /> */}
              </View>
              <View style={styles.inputView} >
                <TextInput  
                  style={styles.inputText}
                  placeholder="Email" 
                  placeholderTextColor="#644263"
                  keyboardType="email-address"
                  value={this.state.email}
                  onChangeText={email => this.setState({email})}
                />
              </View>
              <View style={styles.inputView} >
                <TextInput  
                  style={styles.inputText}
                  placeholder="Password" 
                  placeholderTextColor="#644263"
                  value={this.state.password}
                  onChangeText={password => this.setState({password})}
                  secureTextEntry={true}
                />
              </View>
              <TouchableOpacity
                // disabled={true}
                onPress={
                    async () => {
                    try {
                      // setLoading(true);
                      this.setState({loading:true})
                      const data = {
                        email: this.state.email,
                        password: this.state.password,
                      }
                      await this.login('local',data);
                    } catch (e) {
                      // setError(e.message);
                      this.setState({loading:false})
                      // setLoading(false);
                    }
                  }
                }
                style={styles.loginBtn}>
                <Text style={styles.loginText}>LOGIN</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{marginBottom:20}}>
                <Text style={{fontSize:14,color:'#644263'}}>Forgot Password?</Text>
              </TouchableOpacity>
              
              <Text style={{fontSize:12,color:'#644263',marginBottom:5}}>Sign up with</Text>
              <View style={styles.buttonContainer}>
                <TouchableOpacity
                  onPress={async () => {
                    try {
                      await this.login('facebook');
                    } catch (e) {
                      alert(e.message);
                    }
                  }}
                  // onPress={this.facebookLogin}
                  style={styles.socialContainer}>
                  <Ionicons name="logo-facebook" size={25} style={styles.socialIcon} />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={async () => {
                    try {
                      await this.login('google');
                    } catch (e) {
                      alert(e.message);
                    }
                  }}
                  style={styles.socialContainer}>
                  <Ionicons name="logo-google" size={20} style={styles.socialIcon} />
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() => { 
                  this.setState({modalVisible:true});
                }}
              >
                <Text style={{fontSize:11,color:'#644263'}}>Terms Of Use</Text>
              </TouchableOpacity>

            </View>
          </ImageBackground>
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}
          >
            <View style={styles.modalContainer}>
              <View style={styles.modalView}>
              <ScrollView>
                <Text type="bold" style={styles.modalTitle}>TERMS OF USE</Text>
                <Text style={styles.modalText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed enim elit, rhoncus nec metus vel, posuere ultrices augue. Curabitur congue nisi sit amet dui porttitor, sit amet finibus ante consectetur. Proin nec urna in est rhoncus gravida nec nec lectus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a enim ac elit tincidunt accumsan in at lorem. Nulla ex risus, facilisis non nunc id, ultricies facilisis velit. Integer eu auctor tellus. Proin at accumsan lacus, in laoreet magna. Nam id ultrices purus.
                  Nunc at mollis nunc, at fringilla mi. Duis eros magna, pretium sed eleifend ut, mollis ut velit. Donec eget elit lectus. Donec in lorem vitae purus blandit vehicula et id leo. Sed rutrum odio libero, nec semper orci congue quis. Mauris eget erat velit. In pretium dapibus metus nec ornare. Etiam volutpat lectus a porttitor pulvinar. Morbi efficitur, enim sit amet faucibus pellentesque, tortor ante fringilla felis, non convallis erat leo vel tortor. Sed scelerisque </Text>
              </ScrollView>
              <TouchableOpacity
                  style={{ ...styles.loginBtn }}
                  onPress={() => {
                    this.setState({modalVisible:false});
                  }}
                >
              <Text style={styles.loginText}>I Understand</Text>
            </TouchableOpacity>
              </View>
            </View>
          </Modal>
          <Loading loading={this.state.loading} />
      </View>
  )
}
      
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: DEVICE_WIDTH,
    height: DEVICE_HEIGHT,
    alignItems:'center',
    justifyContent:'center',
  },

  modalTitle: {
    fontSize: 18,
    color: '#644263',
    marginBottom: 20,
    textAlign:'center'
  },
  modalText: {
    marginBottom: 15,
    color: '#644263',
    fontSize: 17
    // textAlign: "center"
  },
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    
    position: 'relative'
    
  },
  modalView: {
    width: "95%",
    margin: 20,
    backgroundColor: "white",
    borderRadius: 5,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
  },
  buttonContainer: {
    flexDirection: 'row',
    textAlign: 'center',
    marginBottom: 30,

  },
  socialContainer: {
    overflow: 'hidden',
    borderRadius: 50,
    backgroundColor: '#A375A3',
    // padding:13,
    height: 40,
    width: 40,
    //  borderRadius: 150 / 2,
     alignItems:'center',
    justifyContent:'center',
    margin: 5
  },
  socialIcon: {
    color: 'white',

  },
  loginBtn:{
    width:"80%",
    backgroundColor:"#A375A3",
    fontSize:18,
    borderRadius:5,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:10,
    marginBottom:10
  },
  loginText: {
    color: 'white',
    letterSpacing: 2,
  },
  inputView:{
    width:"80%",
    backgroundColor:"#fff",
    borderRadius:5,
    height:50,
    marginBottom:5,
    justifyContent:"center",
    padding:20,
    opacity: .5
  },
  inputText:{
    height:50,
    fontSize:18,
    color: '#644263',
  },
  logoContainer: {
    marginBottom: 10,
    alignItems: 'center' ,marginTop: (DEVICE_HEIGHT / 2) - hp('31%')
  },
  button : {
    height: 65,
    backgroundColor: '#A375A3',
    width: 180,
    //  width: "40%",
    margin:5,
    justifyContent:'center',alignItems:'center',
    letterSpacing: 100,
    // justifyContent: 'flex-end',
    // flex: 1,

  },
  contentContainer: {
    // flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backgroundImage: {
      flex: 1,
      resizeMode: 'cover', // or 'stretch'
      // height: hp('110%'),
      position: 'absolute', top: 0, bottom: 0, left: 0, right: 0
  },
  
});
