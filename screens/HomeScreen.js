import React, { Component } from 'react';
import { StyleSheet, View, ScrollView,SafeAreaView,FlatList,TouchableOpacity,TouchableHighlight } from 'react-native';
import { ListItem, Avatar, Card, Image } from 'react-native-elements';

import AppHeader from '../components/AppHeader';
import Text from '../components/CustomText';
import api from '../utils/api';

export default class HomeScreen extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      packages: [],
      services:[],
    }
    this.onPress = this.onPress.bind(this);
    
  }

  onPress(val) {
    console.log(val);
  }
  _isEven(value) {
    if (value%2 == 0)
      return true;
    else
      return false;
  }

  async componentDidMount() {
    await this.setState({ isLoading: true })
    await this.packages();
    await this.services();

    // console.log('service ID:  '+JSON.stringify(this.props.navigation.getParam('service_id')))
}

async services(){
  await api.get('/services').then(async (response) =>  {
    if (response.data.services){
        this.setState({ 'services': response.data.services });
    } else {
        await this.setState({ isLoading: true })
    }  
  }).catch((error)=>{
      console.log(error.response);
      alert(error.message);
   });
}

async packages(){
  await api.get('/packages').then(async (response) =>  {
    if (response.data.packages){
        this.setState({ 'packages': response.data.packages });
        await this.setState({ isLoading: false })

    } else {
        await this.setState({ isLoading: true })
    } 
  }).catch((error)=>{
      console.log(error.response);
      alert(error.message);
  });
}

  // static navigationOptions = ({ navigation }) => ({
  //   // header: <AppHeader showBadge={true} leftAction={() => { return null }} leftLabel={' '} rightAction={() => navigation.navigate('ViewCart')} rightLabel={<Ionicons name={Platform.OS === 'ios' ? 'ios-cart' : 'md-cart'} size={25} />} />
  //   header: <AppHeader/>
  // });
  render() {

    const { navigation } = this.props;
    return (
      <View style={styles.MainContainer}>
        <SafeAreaView style={{flex: 1}}>
          <ScrollView contentInset={{bottom: 30}}>
            <View style={{backgroundColor:'#f5f5f5', paddingBottom:15}}>
              <FlatList
                data={this.state.packages}
                horizontal={true}
                renderItem={({ item, rowData }) => {
                  if(item.status == 'Active') {
                    return (
                      // <Card containerStyle={{ padding: 0, width: 330, marginRight:0 }}>
                        <Image
                          source={{uri: item.promoImages}}
                          style={{ height: 110, width: 330, marginTop:20,resizeMode:'contain', borderRadius:30}}
                        />
                      // </Card>
                    )
                  }
                }}
                keyExtractor={item => item._id.toString()}
              />
            </View>
            <View style={{backgroundColor:'#fff'}}>
              {this.state.services.map((service, i) => {
                if(service.status == 'Enabled') {
                  return (
                    <TouchableOpacity  key={i}
                      onPress={() => {
                        this.props.navigation.navigate('AvailableBeauticians', {
                          service: service.category,
                          service_id: service.id,
                        });
                      }}
                    >
                    <Card containerStyle={[{padding:0, position: "relative"},styles.card]}>
                      <Image
                        source={{uri: service.serviceImage}}
                        style={{ height: 200 }}
                      />
                      <Text style={[styles.serviceText,this._isEven(service.id) ? styles.leftText : styles.rightText]}>
                        {service.category}
                      </Text>
                    </Card>
                    </TouchableOpacity>
                  );
                }
                })
              }
            </View>
          </ScrollView>
        </SafeAreaView>
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1
  },
  serviceText: {
    position: 'absolute',
    top: '50%',
    fontWeight: '500',
    fontSize:22
  },
  rightText: {
    right: 10
    
  },
  leftText: {
    left: 10
  },
  row: {
    flex: 1,
    justifyContent: "space-around"
  },
  card: {
    borderRadius: 8,
    borderWidth: 0, // Remove Border
    elevation:0,
    shadowColor: 'rgba(0,0,0, .2)',
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0, //default is 1
    shadowRadius: 0//default is 1
  }
});