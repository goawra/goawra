import React, { Component } from 'react';
import { StyleSheet, View, ScrollView,SafeAreaView,FlatList,TouchableOpacity,TouchableHighlight } from 'react-native';
import { ListItem, Avatar, Card, Image } from 'react-native-elements';

import AppHeader from '../components/AppHeader';
import Text from '../components/CustomText';
import api from '../utils/api';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default class BookingHistoryScreen extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      list: [
        {
          name: 'Amy Farha',
          avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
          subtitle: 'Vice President'
        },
        {
          name: 'Chris Jackson',
          avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
          subtitle: 'Vice Chairman'
        },
        {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },
          {
            name: 'Chris Jackson',
            avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
            subtitle: 'Vice Chairman'
          },

      ]
    }
    
  }



  async componentDidMount() {
    // await this.setState({ isLoading: true })
    // await api.get('/packages').then(async (response) =>  {
    //   if (response.data.packages && response.data.packages.status != 'Active'){
    //       this.setState({ 'packages': response.data.packages });
    //       // await this.setState({ isLoading: false })
    //       // console.log('DATA---------- LOADED');
    //       // console.log('loading----------'+this.state.isLoading);
    //       console.log(response.data.packages);
    //   } else {
    //       await this.setState({ isLoading: true })
    //       // console.log('loading----------'+this.state.isLoading);
    //   }
    //     // console.log(this.state.beauticians[0]);
        
    // }).catch((error)=>{
    //     console.log(error.response);
    //     alert(error.message);
    //  });
    console.log(this.state.list)

    // console.log('service ID:  '+JSON.stringify(this.props.navigation.getParam('service_id')))
}
    keyExtractor = (item , index) => index.toString();
    renderItem = ({ item }) => {
        <ListItem 
            title={item.name}
            // subtitle={'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'}
            bottomDivider={true}
        />
    }

  render() {
        return(
            <SafeAreaView style={styles.container}>
                <FlatList 
                    data={this.state.list}
                    // renderItem={this.renderItem}
                    renderItem={({ item, index }) => {
                        let evenRow = index.toString() % 2 == 0;
                        console.log(evenRow)
                        return(
                            <View style={[styles.listView, evenRow && styles.evenColor]}>
                              <View style={{flexDirection:'row', justifyContent: 'space-between'}}>
                                  <View style={{paddingRight:10,paddingTop:10}}>
                                    <MaterialCommunityIcons name="clock-outline" size={50} color="black" />
                                  </View>
                                  <View>
                                    <View style={styles.title}>
                                        <View style={styles.titleName}>
                                            <Text style={{fontSize:20}}>{item.name}</Text> 
                                        </View>
                                        <View style={styles.date}>
                                            <Text>06/06/20</Text>
                                        </View>
                                    </View>
                                    <View style={styles.content}>
                                        <Text style={{fontSize:14}}>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </Text>
                                        <Text style={{marginTop:10,fontSize:14}}>
                                          <MaterialCommunityIcons name="clock-outline" size={15} color="black" />
                                          6 days ago | <Text style={{color:'#0BB606'}}>Completed</Text>
                                        </Text>
                                    </View>
                                  </View>
                                </View>
                            </View>
                           
                        )
                    }}
                    keyExtractor={this.keyExtractor}
                />
            </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  listView:{
    paddingRight:30,
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 10,
    backgroundColor:'#fff',
  },
  listItem:{
    flexWrap:'wrap',
    alignItems: 'center',
    // flexDirection: 'row',
    backgroundColor:'#fff',
    
  },
  evenColor: {
    backgroundColor: '#F5F5F5'
    // backgroundColor: '#000'
  },
  content: {
    // flexDirection: 'row',
    paddingTop: 5,
    width:wp('75%')
  },
  date: {
    textAlign: 'right',
    fontSize:14
  },
  title:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    
  },
  titleName:{
    textAlign: 'left',
    fontSize:16
  }
});