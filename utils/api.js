import axios from 'axios';
import CONFIG from '../config/config';
export default axios.create({
    baseURL: CONFIG.BASE_URL, // or process.env.BASE_URL
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    }
});

