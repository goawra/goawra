import React, { Component } from 'react'
import {
    Dimensions,
    Image,
    View,
    FastImage,
    TouchableOpacity,
    StyleSheet,
    Platform
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Ionicons } from '@expo/vector-icons'
import Text from './CustomText';

const styles = StyleSheet.create({
    serviceNameCont: {
        borderBottomWidth: 1,
        borderBottomColor: '#e4e6e8',
        padding: 18,
        fontSize: 18,
        flexDirection: 'row',
        alignItems: 'center'
    },
    buttonCont: {
        position: 'absolute',
		right: 10,
		bottom: 2.5
    }
})

export default class ServiceListItem extends Component {
    constructor(props) {
        super();

        this.state = {
            selected: false
        }
    }

    handleSelectService = (id) => {
        this.setState(prevState => ({
        	selected: !prevState.selected
        }));
        this.props.selectService(id);
    }

    render() {
        return (
            <TouchableOpacity onPress={() => this.handleSelectService(this.props.id)}>
                <View style={styles.serviceNameCont}>
                    <Text>{this.props.name}</Text>
                    <View style={styles.buttonCont}>
                        {
                            this.state.selected ? (
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-checkmark-circle-outline' : 'md-checkmark-circle'} size={25} style={styles.buttonCont} />
                            ) : (
                                <Ionicons name={Platform.OS === 'ios' ? 'ios-add-circle-outline' : 'md-add-circle'} size={25} style={styles.buttonCont} />
                                )
                        }
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}
