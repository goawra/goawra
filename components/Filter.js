import React, {Component} from 'react';
import { StyleSheet, Text, View, FlatList, TouchableHighlight, TextInput,Dimensions} from 'react-native';
import { SearchBar,Icon } from 'react-native-elements';

const DEVICE_WIDTH = Dimensions.get(`window`).width;
const CONTAINER_WIDTH = DEVICE_WIDTH / 1.3 - 80;
export default class Filter extends Component {
    state = {
      search: '',
    };
  
    updateSearch = (search) => {
      this.setState({ search });
    };
  
    render() {
      const { search } = this.state;
  
      return (
        <SearchBar
          placeholder={CONTAINER_WIDTH}
          onChangeText={this.updateSearch}
          value={search}
          platform='android'
          searchIcon={()=>
            <Icon
                name='sound-mix'
                type='entypo'
                color='#929292'
                style={{transform: [{rotate: '90deg'}]}}
            />
        }
        />
      );
    }
  }