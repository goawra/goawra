import React, { Component } from 'react';
import {
  Dimensions,
  Image,
  View,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Text from './CustomText';

const DEVICE_WIDTH = Dimensions.get(`window`).width;
const DEVICE_HEIGHT = Dimensions.get(`window`).height;
const MODAL_WIDTH = Dimensions.get(`window`).width * .9;
const MODAL_HEIGHT = Dimensions.get(`window`).height * .85;
const CONTAINER_WIDTH = DEVICE_WIDTH / 2 - 10; // (margin * 2) + containerguttergutter on one side
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    width: CONTAINER_WIDTH,
    //height: 280,  
    borderBottomWidth: 4,
    borderColor: '#333',
    margin: 5,
    backgroundColor: '#FFFFFF'
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  buttonDisabled: {
    opacity: .5,
  },
  buttonCaption: {
    marginRight: 5,
    color: '#000',
    fontSize: 18
  },
  skipCaption: {
    opacity: .5,
    marginRight: 5
  },
  buttonCaptionWhite: {
    color: 'white'
  },
  buttonCaptionBlack: {
    color: '#000'
  },
  bullet: {
    backgroundColor: '#A375A3',
    padding: 3,
    marginLeft: 2,
    marginRight: 2,
    height: 10,
    width: 10,
    borderRadius: 100,
    borderWidth: 1,
    borderColor: '#A375A3'
  },
  activeBullet: {
    backgroundColor: '#fff'
  }
});


export default class BulletNavigation extends Component {
  constructor(props) {
    super();

    this.state = {
      selected: false,
      modalVisible: false,
      // params : (this.props.params) ? this.props.params : ''
      // selectedServices : ''
    };
  }

  toggleSelect(id) {
    this.setState(prevState => ({ selected: !prevState.selected }));
    //use id variable here
  }

  toggleModal(visible) {
    this.setState(prevState => ({ modalVisible: !prevState.modalVisible }));
  }

  generateBullets(n) {
    var bullets = [];

    for (let i = 1; i <= n; i++) {
      let bulletStyle = [styles.bullet]

      if (i == this.props.active) {
        bulletStyle.push(styles.activeBullet);
      }

      bullets.push(<TouchableOpacity style={bulletStyle} onPress={() => { this.toggleModal() }} activeOpacity={1} key={i}></TouchableOpacity>);
    }

    return bullets;
  }


  skipButton() {
    if (this.props.skip == "true") {
      return (
        <View style={{ position: 'absolute', top: 20, left: 20 }} >
          <TouchableOpacity onPress={() => { this.props.navigation.navigate(this.props.next) }}>
            <View style={styles.button}>
              <Text style={[styles.buttonCaption]}>Skip</Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    }
  }
  backButton() {
    if (this.props.back == "true") {
      style = [styles.button];

      // if (this.props.disable == true) {
      //   style.push(styles.buttonDisabled);
      // }
      const buttonColor = (this.props.active == 1) ? styles.buttonCaptionWhite : styles.buttonCaptionBlack;
      const arrowImage = (this.props.active == 1) ? require('../assets/images/icons/arrow-white.png') : require('../assets/images/icons/arrow-black.png');

      return (
        <View style={{ position: 'absolute', top: 30, left: 20 }} >
          <TouchableOpacity onPress={() => { this.props.navigation.goBack() }} >
            <View style={style}>
              <Image source={arrowImage} style= {{height:17,marginRight: 6,transform: [{ rotate: '180deg'}]}} resizeMode='contain'/>
              <Text style={[styles.buttonCaption,buttonColor]}>Back</Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    }
  }

  nextButton() {
    console.log(this.props.params)
    

    style = [styles.button];

    if (this.props.disable == true) {
      style.push(styles.buttonDisabled);
    }
    const buttonColor = (this.props.active == 1) ? styles.buttonCaptionWhite : '';
    const arrowImage = (this.props.active == 1) ? require('../assets/images/icons/arrow-white.png') : require('../assets/images/icons/arrow-black.png');

    return (
      <View style={{ position: 'absolute', top: 30, right: 20 }} >
        <TouchableOpacity disabled={this.props.disable} onPress={() => { this.props.navigation.navigate(this.props.next,{params: this.props.params } ) }} >
          <View style={style}>
            <Text style={[styles.buttonCaption,buttonColor]}>Next</Text>
            <Image source={arrowImage} style= {{height:17}} resizeMode='contain'/>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
  empty(){}

  render() {
    
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'center', height: 80, alignItems: 'center' }}>
        {this.skipButton()}
        {this.backButton()}
        {this.generateBullets(this.props.max)}
        {(this.props.max != this.props.active) ? this.nextButton() : this.empty() }
        {/* // {this.nextButton()} */}
        
      </View>
    )
  }
}
