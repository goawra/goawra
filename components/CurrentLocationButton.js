import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    Text,
    TouchableOpacity
} from 'react-native';
import  { MaterialIcons } from '@expo/vector-icons';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export const CurrentLocationButton = function(props) {
    const cb = props.cb ? props.cb : console.log('Callback function not passed to CurrentLocationButton.')
    const bottom = props.bottom ? props.bottom : 105;
    console.log(HEIGHT);
    return(
        
        
        <TouchableOpacity style={[styles.container,{bottom: bottom}]}
        onPress={props.onPress}> 
            <MaterialIcons
                name="my-location"
                color="#000"
                size={25}
                // onPress = {() => _mapView.animateToCoordinate({
                //     latitude: LATITUDE,
                //     longitude: LONGITUDE
                //   }, 1000)}
                // onPress={() => {}}
                
            />
            
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        zIndex: 9,
        position: 'absolute',
        width: 45,
        height: 45,
        backgroundColor: '#fff',
        left: WIDTH - 50,
        // bottom: 105,
        borderRadius: 50,
        shadowColor: '#000',
        elevation: 7,
        shadowRadius: 5,
        // shadowOpacity: 1.0,
        justifyContent: 'space-around',
        alignItems: 'center',

    }
});