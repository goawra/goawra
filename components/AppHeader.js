import React, { Component } from 'react';
import {
    Dimensions,
    AppRegistry,
    StyleSheet,
    View,
    Text,
    Image,
    Button,
    Alert,
    TouchableWithoutFeedback,
    TouchableOpacity
} from 'react-native';
import { Icon } from 'react-native-elements';

const DEVICE_WIDTH = Dimensions.get(`window`).width;
const CONTAINER_WIDTH = DEVICE_WIDTH / 1.3 - 80;

const styles = StyleSheet.create({
    backButton: {
        position: 'absolute', 
        top: 50,  
        left: 20
    }
});


export default class AppHeader extends Component {
  render() {
    return (
        <TouchableOpacity style={styles.backButton} onPress={() => {this.props.navigation.goBack()}}>
            <Icon
              name="menu"
              size={30}
              type="entypo"
              color='#000'
              iconStyle={{ paddingLeft: 10 }}
              // onPress={navigation.toggleDrawer}
            />
            <Icon
              name="chevron-left"
              size={30}
              type="entypo"
              color='#000'
              iconStyle={{ paddingLeft: 10 }}
              // onPress={navigation.toggleDrawer}
            />
        </TouchableOpacity>
    )   
  }
}
