//This is an example code for Navigation Drawer with Custom Side bar//
import React, { Component } from 'react';
import { View, StyleSheet, Image, TouchableOpacity,Dimensions,ScrollView } from 'react-native';
import { Icon } from 'react-native-elements';
import SafeAreaView from 'react-native-safe-area-view';

import Text from './CustomText';
import AsyncStorage from '@react-native-community/async-storage';
const { width: screenWidth, height: screenHeight } = Dimensions.get('window');

export default class CustomSidebarMenu extends Component {
  constructor() {
    super();
    this.state = {
      userData : []
    };
    this.items = [
      {
        navOptionThumb: require('../assets/images/icons/home-grey.png'),
        navOptionName: 'Home',
        screenToNavigate: 'Home',
      },
      {
        navOptionThumb: require('../assets/images/icons/booking-black.png'),
        navOptionName: 'Booking History',
        screenToNavigate: 'BookingHistory',
      },
      {
        navOptionThumb: require('../assets/images/icons/schedule.png'),
        navOptionName: 'Schedule',
        screenToNavigate: 'Schedule',
      },
      {
        navOptionThumb: require('../assets/images/icons/wallet.png'),
        navOptionName: 'Wallet',
        screenToNavigate: 'Wallet',
      },
      {
        navOptionThumb: require('../assets/images/icons/salon-partner.png'),
        navOptionName: 'Salon Partners',
        screenToNavigate: 'SalonPartners',
      },
      {
        navOptionThumb: require('../assets/images/icons/notif.png'),
        navOptionName: 'Notifications',
        screenToNavigate: 'Notification',
      },
      {
        navOptionThumb: require('../assets/images/icons/help-center.png'),
        navOptionName: 'Help Center',
        screenToNavigate: 'HelpCenter',
      },
      {
        navOptionThumb: require('../assets/images/icons/settings.png'),
        navOptionName: 'Settings',
        screenToNavigate: 'Settings',
      },
      {
        navOptionThumb: require('../assets/images/icons/share-thoughts.png'),
        navOptionName: 'Share Thoughts',
        screenToNavigate: 'ShareThoughts',
      },
      {
        navOptionThumb: require('../assets/images/icons/share-thoughts.png'),
        navOptionName: 'FAQ',
        screenToNavigate: 'Faq',
      },
    ];
  }
  async componentDidMount(){
    const userData = await AsyncStorage.getItem('@userData');
    let parseData = JSON.parse(userData);
    
    await this.setState({userData:parseData})
    
    
    
  }
  render() {
    let userData = this.state.userData;
    
    return (
      <View style={styles.sideMenuContainer}>
        <View style={styles.avatar}>
            <Image 
                source={(userData.photoUrl) ? {uri:userData.photoUrl} : require('../assets/images/avatar.png')}
                style={{ width: 100, height:100 ,borderRadius: 150 / 2}}
            />
        </View>
        <TouchableOpacity
          onPress={() => {this.props.navigation.navigate('Profile')}}
        >
            <Text>Edit Profile</Text>
        </TouchableOpacity>
        <Text style={styles.textName}>{userData.FullName}</Text>
        {/*Divider between Top Image and Sidebar Option*/}
        <View
          style={{
            width: '100%',
            height: 1,
            backgroundColor: '#e2e2e2',
            marginTop: 15,
          }}
        />
        {/*Setting up Navigation Options from option array using loop*/}
        
        <View style={{ width: '100%',  flex:1}}>
        <ScrollView>
          {this.items.map((item, key) => (
            <View key={key} style={{borderBottomColor: '#e2e2e2',borderBottomWidth: 1,}}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('profile');
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingTop: 10,
                paddingBottom: 10,
                // marginBottom: 10,
              }}
            >
              <View style={{ marginRight: 10, marginLeft: 25 }}>
                {/* <Icon name="camera" size={25} color="#808080" /> */}
                <Image
                    source={(item.navOptionThumb)}
                    style={{ width: 25,height:25 }}
                    resizeMode="contain"
                />
              </View>
              <TouchableOpacity
                onPress={() => {
                    this.props.navigation.navigate(item.screenToNavigate);
                }}>
                <Text
                  style={{
                    fontSize: 16,
                  }}
                  onPress={() => {
                    this.props.navigation.navigate(item.screenToNavigate);
                  }}>
                  {item.navOptionName}
                </Text>
              </TouchableOpacity>
              
            </View>
            </TouchableOpacity>
            </View>
          ))}
          
          <TouchableOpacity
                onPress={async () => {
                    this.props.navigation.navigate('Auth');
                    await AsyncStorage.removeItem('@userData')
                }}
              >
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingTop: 10,
                paddingBottom: 10,
              }}
              
              >
              <View style={{ marginRight: 10, marginLeft: 20 }}>
                <Icon
                  name='log-out'
                  type='entypo'
                  // color='#517fa4'
                  style={{transform: [{rotate: '180deg'}]}}
                />
              </View>
              <Text style={{fontSize: 18}}>
                Logout
              </Text>
            </View>
            </TouchableOpacity>
            </ScrollView>
            </View>
            <View style={styles.footerContainer}>
              <Text>Rate us</Text>
            </View>
        
      </View>
    );
  }
}

<View style={{borderWidth: 4,borderColor:'#A685B5', borderRadius:60}}></View>
const styles = StyleSheet.create({
    avatar: {
        // flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 4,borderColor:'#A685B5', borderRadius:60,
        backgroundColor:'#644263'
      },
      textName: {
        fontSize: 23,
        color: '#2C2C2C'
      },
  sideMenuContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
    alignItems: 'center',
    marginTop: 50,
  },
  sideMenuProfileIcon: {
    resizeMode: 'center',
   
    marginTop: 20,
    borderRadius: 150 / 2,
  },
  drawerItem: {
    backgroundColor: '#000',
  },
  footerContainer: {
    flex:.1,
    paddingBottom: 40,
    flexDirection: 'row', alignItems: 'center'
    // bottom: 0,
    
  }
});