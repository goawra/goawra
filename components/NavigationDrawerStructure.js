import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    Image,
    TouchableOpacity,
    Platform,
    Text,
  } from 'react-native';
import { Icon } from 'react-native-elements';

class NavigationDrawerStructure extends Component {
    //Top Navigation Header with Donute Button
    toggleDrawer = () => {
      //Props to open/close the drawer
      this.props.navigationProps.toggleDrawer();
    };
    render() {
      return (
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
            {/*Donute Button Image */}
            {/* <Image
              source={{uri:'https://aboutreact.com/wp-content/uploads/2018/07/drawer-150x150.png'}}
              style={{ width: 25, height: 25, marginLeft: 5 }}
            /> */}
            <Icon
              name="menu"
              size={30}
              type="entypo"
              color='#fff'
              iconStyle={{ paddingLeft: 10 }}
              // onPress={navigation.toggleDrawer}
            />
          </TouchableOpacity>
        </View>
      );
    }
  }

  export default NavigationDrawerStructure;