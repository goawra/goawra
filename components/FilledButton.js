import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import Text from './CustomText';

export function FilledButton({title, style, onPress}) {
  return (
    <TouchableOpacity
      // style={[styles.container, style, {backgroundColor: colors.primary}]}
      style={[styles.container, style]}
      onPress={onPress}>
      <Text style={styles.text}>{title.toUpperCase()}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    // width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 13,
    borderRadius: 4,
  },
  text: {
    color: 'white',
    fontSize: 15,
    lineHeight: 18,
    letterSpacing: 2,
    textAlign:'center'

  },
});
