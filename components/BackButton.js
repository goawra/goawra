import React, { Component } from 'react';
import {
    Dimensions,
    AppRegistry,
    StyleSheet,
    View,
    Text,
    Image,
    Button,
    Alert,
    TouchableWithoutFeedback,
    TouchableOpacity
} from 'react-native';
import { Icon } from 'react-native-elements'

const DEVICE_WIDTH = Dimensions.get(`window`).width;
const CONTAINER_WIDTH = DEVICE_WIDTH / 1.3 - 80;

const styles = StyleSheet.create({
    
});


export default class BackButton extends Component {
  render() {
    return (
        <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity onPress={() => {this.props.navigation.goBack()}}>
                
                    <Icon
                        name='sc-telegram'
                        type='evilicon'
                        color='#517fa4'
                    />
                    {/* <Image source={require('../assets/images/back-button.png')} /> */}
                
            </TouchableOpacity>
        </View>
    )   
  }
}
