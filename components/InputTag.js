import React, { Component } from 'react';
import {
  Dimensions,
  StyleSheet,
  View
} from 'react-native';
import {Icon} from 'react-native-elements';

import TagInput from 'react-native-tags-input';

const mainColor = '#644263';

export default class InputTag extends Component {
  constructor(props) {
    super();
    this.state = {
      tags: {
        tag: '',
        tagsArray: []
      },
      tagsColor: mainColor,
      tagsText: '#fff',
    };
  }
  
  updateTagState = (state) => {
      this.setState({
        tags: state
      })
      // this.handleSkills(state)
    };
    

  handleSkills= (state) => {
      // this.setState(prevState => ({
      //   tags: !prevState.tags
      // }));
      // this.props.data(this.state.tags);
  }
    

  render() {
    return (
      <View style={styles.container}>
        <TagInput 
        
          updateState={this.updateTagState}
          tags={this.state.tags}
          placeholder="Other Skills:"                            
          // label='Press comma & space to add a tag'
          labelStyle={{color: '#E6E6E6'}}
          // leftElement={<Icon name={'tag-multiple'} type={'material-community'} color={this.state.tagsText}/>}
          leftElementContainerStyle={{marginLeft: 3}}
          // containerStyle={{width: (Dimensions.get('window').width - 40)}}
          inputContainerStyle={styles.textInput}
          // inputStyle={{color: this.state.tagsText}}
          // onFocus={() => this.setState({tagsColor: '#fff', tagsText: mainColor})}
          // onBlur={() => this.setState({tagsColor: mainColor, tagsText: '#fff'})}
          autoCorrect={false}
          tagStyle={styles.tag}
          // tagTextStyle={styles.tagText}
          keysForTag={', '}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: mainColor,
  },
  textInput: {
      // height: 40,
      // borderWidth: 1,
      marginTop: 5,
      padding: 5,
    },
    tag: {
        backgroundColor: '#E6E6E6',
        borderRadius: 5,
        // padding:
        height: 45,
        paddingRight:5,
        marginTop: 0
      },
    tagText: {
        color: mainColor
      },
});