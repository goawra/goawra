import React from 'react';
import {View, StyleSheet, ActivityIndicator, Image} from 'react-native';
import Text from './CustomText';



export function RegistrationComplete({status,navigation}) {

  if (!status) {
    return <View />;
    
  }
  setTimeout(() => {
    navigation.navigate('LoginSelection');
  }, 3000);
  return (
    <View style={styles.overlay}>
      <View style={styles.container}>
        <Image
            source={require('../assets/images/GoAwra-logo.png')}
            // style={{ width: 208, height: 185 }}
            style={styles.logo}
        />
        <View style={[styles.textContainer,{marginBottom:30}]}>
            <Text style={styles.text}>Registration complete!</Text>
        </View>
        <View style={styles.textContainer}>
            <Text style={styles.text}>Someone from GoAwra will</Text>
            <Text style={styles.text}>Contact you for verification</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  overlay: {
    ...StyleSheet.absoluteFill,
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    // padding: 20,
    // borderRadius: 8,
  },
  text: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 22,
  },
  textContainer: {
    textAlign: 'center',
    fontWeight: '500',
  },
  logo: {
    width: 208,
    height: 185,
    // flex: 1,
  alignItems: "center",
  // // resizeMode: "cover",
  justifyContent: "center",
  
  resizeMode:'contain'
},
});
