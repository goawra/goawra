import React, { Component, useState, useEffect } from 'react';
import { View, Text,Image } from 'react-native';
// import AppLoading from './components/AppLoading';
import { AppLoading } from 'expo';
import RootNavigator  from './navigation/RootNavigator';

import { ThemeProvider } from 'react-native-elements';
import vectorFonts from './helpers/vector-fonts';
import { cacheImages, cacheFonts } from './helpers/AssetsCaching';
import { SafeAreaProvider,initialWindowMetrics } from 'react-native-safe-area-context';

// import {AuthContext} from './contexts/AuthContext';
// import {useAuth} from './hooks/useAuth';
// import {UserContext} from './contexts/UserContext';
// import {ThemeContext} from './contexts/ThemeContext';
// import Splash from './screens/SplashScreen';

// console.disableYellowBox = true;

// export default App = () => {
//   const [dataLoaded,setDataLoaded] = useState(false);
 
// const loadAssetsAsync = async () => {
//   const fontAssets = cacheFonts([
//     ...vectorFonts,
//     { 'Optima': require('./assets/fonts/Optima.ttf') },
//     { 'Optima-Italic': require('./assets/fonts/Optima-Italic.ttf') },
//     { 'Optima-Medium': require('./assets/fonts/Optima-Medium.ttf') },
//     { 'Optima-Bold': require('./assets/fonts/Optima-Bold.ttf') },
//   ]);
//   await Promise.all([...fontAssets]);
// };

//   useEffect(() => {
//     loadAssetsAsync().then(() => setDataLoaded(true)).catch(e => {
//       console.warn("Error loading assets");
//       setDataLoaded(true);
//     });
//   }, []);
  
// if (!dataLoaded) {
//   return (
//     <AppLoading
//       startAsync={loadAssetsAsync}
//       onFinish={() => setIsReady(true)}
//       onError={console.warn}
//     />
//   );
// }
// <RootNavigator />

// };





export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false
    }
  }

  async UNSAFE_componentWillMount() {
    
  }

  loadAssetsAsync = async () => {
    const fontAssets = cacheFonts([
      ...vectorFonts,
      { 'Optima': require('./assets/fonts/Optima.ttf') },
      { 'Optima-Italic': require('./assets/fonts/Optima-Italic.ttf') },
      { 'Optima-Medium': require('./assets/fonts/Optima-Medium.ttf') },
      { 'Optima-Bold': require('./assets/fonts/Optima-Bold.ttf') },
    ]);
    await Promise.all([...fontAssets]);
    console.log('Assets Load Successfully');
  };

  render(){
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this.loadAssetsAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }else{
      return (
        <SafeAreaProvider initialMetrics={initialWindowMetrics} >
          <ThemeProvider >
              <RootNavigator />
          </ThemeProvider>
        </SafeAreaProvider>
        
      );
    }
    
  }
  
  
};




