import { Platform } from 'react-native';
const CONFIG = {}

CONFIG.BASE_URL = (Platform.OS === "ios" ? 'http://localhost:4200': 'http://10.0.2.2:4200');
// CONFIG.BASE_URL = 'http://192.168.100.146:4200';
// CONFIG.BASE_URL = 'http://api.goawra.com';
CONFIG.FACEBOOK_APP_ID = '388983588673825';
CONFIG.google_iosClientId = '973138177475-jhd285qerebt72hvns08ndc9ir0f7g90.apps.googleusercontent.com';
CONFIG.google_androidClientId = '973138177475-9cdub20jnn71nku4hhqucd8nfu7g5hss.apps.googleusercontent.com';
CONFIG.google_expoClientId = '973138177475-0aop2e5hapam9njdfr4vsd9t444ocler.apps.googleusercontent.com';

module.exports = CONFIG;